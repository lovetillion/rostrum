# Notes on cross compilation

## Testing with Wine

To test the Windows target, install wine and add it to the `CARGO_TARGET_X86_64_PC_WINDOWS_GNU_RUNNER` environment variable.

**Example**

`CARGO_TARGET_X86_64_PC_WINDOWS_GNU_RUNNER=wine64 cargo test --target=x86_64-pc-windows-gnu`

The C++ runtime DLLs need to be available. On OS X, you install `brew install mingw-w64` and add to the environment variable `WINEPATH`. On debian, install
`gcc-mingw-w64-x86-64-posix` and `g++-mingw-w64-x86-64-posix`.

Note that WINEPATH separates paths with semicolon (;), not (:) as normal for unix environments.

**Example OS X**

`export WINEPATH="/usr/local/Cellar/mingw-w64/10.0.0_3/toolchain-x86_64/x86_64-w64-mingw32/lib/;/usr/local/Cellar/mingw-w64/10.0.0_3/toolchain-x86_64/x86_64-w64-mingw32/bin/"`

**Example Debian**

`export WINEPATH="/usr/lib/gcc/x86_64-w64-mingw32/10-posix/;/usr/x86_64-w64-mingw32/lib/`

The package that contains most of the runtime DLL's needed is `gcc-mingw-w64-x86-64-posix-runtime`, a dependency off `gcc-mingw-w64-x86-64-posix`.

The `libwinpthread-1.dll` dependency is in `/usr/x86_64-w64-mingw32/lib/`, installed by package `mingw-w64-x86-64-dev`.

## GCC for linux

You need to install the `posix` verison of gcc/g++!

`apt-get install gcc-mingw-w64-x86-64-posix g++-mingw-w64-x86-64-posix`

If the default version (not posix) is install, you will run into stuff like "mutex" header not existing.

