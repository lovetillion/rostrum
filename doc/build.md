# Building from source

Install a recent version of Rust. A good tool to install Rust is [rustup](https://rustup.rs/).

**Minimum supported Rust version is 1.74**. To check what rust version you
have installed, run `rustc --version`. Trying to build with an older version
usually gives weird build errors.

## Other dependencies
Also the packages `clang` and `cmake` are required to build the database used, rocksdb.

### Debian
```bash
$ sudo apt update
$ sudo apt install clang cmake build-essential # for building 'rust-rocksdb'
```

### OS X

When using [brew](https://brew.sh/).

```bash
$ brew install cmake
```

## First build

Clone and build release optimized binary as follows:

```bash
$ git clone https://gitlab.com/bitcoinunlimited/rostrum.git
$ cd rostrum
$ cargo build --release
```

Use build command `cargo build --release` for Bitcoin Cash. Use
`cargo build --release --features=nexa` for Nexa.

To verify which blockchain network the binary is built for, you can run
`rostrum --version`. It should say `rostrum x.x.x built for nexa` for Nexa or
`rostrum x.x.x built for bitcoin` for Bitcoin Cash.

## Docker-based installation from source

Note: currently Docker installation links statically

Note: health check only works if Prometheus is running on port 4224 inside container

See [list of configuration arguments](configuration).

```bash
$ mkdir db

$ # To build rostrum for Bitcoin Cash
$ docker build -t rostrum-app .

$ # To build rostrum for Nexa
$ docker build --build-arg BUILD_FEATURES=nexa -t rostrum-app .

$ docker run --network host \
             --volume $HOME/.bitcoin:/home/user/.bitcoin:ro \
             --volume $PWD/db:/home/user/db \
             --env ROSTRUM_VERBOSE=4 \
             --env ROSTRUM_TIMESTAMP=true \
             --env ROSTRUM_DB_DIR=/home/user/db \
             --rm -i -t rostrum-app
```

If not using the host-network, you probably want to expose the ports for electrs and Prometheus like so:

```bash
$ docker run --volume $HOME/.bitcoin:/home/user/.bitcoin:ro \
             --volume $PWD/db:/home/user/db \
             --env ROSTRUM_VERBOSE=4 \
             --env ROSTRUM_TIMESTAMP=true \
             --env ROSTRUM_DB_DIR=/home/user/db \
             --env ROSTRUM_ELECTRUM_RPC_ADDR=0.0.0.0:50001 \
             --env ROSTRUM_MONITORING_ADDR=0.0.0.0:4224 \
             --rm -i -t rostrum-app
```

To access the server from outside Docker, add `-p 50001:50001 -p 4224:4224` but be aware of the security risks. Good practice is to group containers that needs access to the server inside the same Docker network and not expose the ports to the outside world.
