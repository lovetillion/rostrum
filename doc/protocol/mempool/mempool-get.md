## mempool.get

Fetch a list of transactions in the mempool

**Signature**

> Function:
> blockchain.mempool.get(filter (optional))
>
> -   *filter*
>
>     Filter what transactions are returned. Filter is a dictionary with the following options as keys:
>
>     - scriptsig - Value is hex string. Include transactions that partially matches scriptsig as inputs.
>
> Version added:
> Rostrum 10.1
>

**Result**

> Dictionary with transaction list

 **Example Results**

    {
        "transactions":  ["6ba738f581968c915a097015fc32d4c951a222d8209a21b0163399bf57622027", "dbbe9a005b0f53eea5b63e3a4f5a36758cd736259d8036c39a4e5a3d7838b1a5"]
    }

