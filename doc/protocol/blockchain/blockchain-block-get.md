## blockchain.block.get

Return the full block with given hash or at the given height.

**Signature**

> Function:
> blockchain.block.get(height_or_hash)
>
> Version added:
> Rostrum 8.1
>
> *hash_or_height*
>
> > The blockhash as hex encoded string or the height of the block, a non-negative integer.
>

**Result**

> The raw block as a hexadecimal string.
