## blockchain.scripthash.subscribe

Subscribe to a script hash.

**Signature**

> Function:
> blockchain.scripthash.subscribe(scripthash)
>
> Version added:
> 1.1
>
> *scripthash*
>
> > The script hash as a hexadecimal string.

**Result**

> The `status <status>` of the script
> hash.

**Notifications**

> The client will receive a notification when the
> `status <status>` of the script hash
> changes. Its signature is
>
> > Function:
> > blockchain.scripthash.subscribe(scripthash, status)