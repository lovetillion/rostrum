## blockchain.utxo.get

Returns data on a specified output of specific transaction. Returns error
if transaction or output does not exist.

If the output is spent, information about the spender is provided. This allows
a SPV client to call `blockchain.transaction.get\_merkle` to generate a merkle
branch, proving that it is spent.

=== "Bitcoin Cash"

    **Signature**

    >  Function: `blockchain.utxo.get(tx_hash, output_index)`
    >
    >  Version added: Rostrum 7.0
    >
    >  Update Rostrum 8.1: Add token details and scriptpukey
    >
    > *tx_hash*
    >
    > > The transaction ID as hexadecimal string.
    >
    > *output\_index*
    >
    > > The vout position in the transaction.

    **Result**

    > A dictionary with the following keys:
    >
    > -  *status*
    >
    >    State of the utxo. A string that is "spent" or "unspent".
    >
    > -  *height*
    >
    >    The height the utxo was confirmed in. If it is unconfirmed, the
    >    value is 0 if all inputs are confirmed, and -1 otherwise.
    >
    > -  *value*
    >
    >    The output’s value in minimum coin units (satoshis).
    >
    > -  *scripthash*
    >
    >    The scriphash of the output scriptPubKey.
    >
    > -  *scriptpubkey*
    >
    >    The locking script of the utxo as hex.
    >
    > -  *spent*
    >
    >    The transaction spending the utxo with the following keys:
    >
    > >  -  *tx\_pos*
    > >
    > >    The zero-based index of the input in the transaction’s list of inputs. Null if utxo is unspent.
    > >
    > > -  *tx_hash*
    > >
    > >    The transaction ID. Null if utxo is unspent.
    > >
    > > -  *height*
    > >
    > >    The height the transaction was confirmed in. If it is unconfirmed, the
    > >    value is 0 if all inputs are confirmed, and -1 otherwise. Null if utxo is unspent.
    >
    > -  *spent*
    >
    >    The transaction spending the utxo with the following keys:
    >
    > -  *token_id*
    >
    >    The token id as hex. Null if none.
    >
    > -  *token_amount*
    >
    >    The amount of token in utxo. Null if none.
    >
    > -  *commitment*
    >
    >    The token commitment. Null if none.
    >
    > -  *token_bitfield*
    >
    >    The token bitfield. Null if none.

    **Example Results**

        {
            "addresses": [
                "bchreg:qzpdwa6c69vwnlj8h7q72sff7nk4tm8mfvktd2hhud",
                "bchreg:zzpdwa6c69vwnlj8h7q72sff7nk4tm8mfv3p75e3r7"
            ],
            "amount": 100000000,
            "commitment": "",
            "height": 0,
            "scripthash": "4cb67fb08b4fd1b2f6b35cb7a785f65b6b00bbb34c84d982775dc20577dbc47f",
            "scriptpubkey": "76a91482d77758d158e9fe47bf81e54129f4ed55ecfb4b88ac",
            "spent": {
                "height": 0,
                "tx_hash": "39e0533673aab846837b9ca5959cc545a87c7be0f6f85a4b6cf2706599eb9fee",
                "tx_pos": 0
            },
            "status": "spent",
            "token_amount": 42,
            "token_bitfield": 16,
            "token_id": "a0482e92a868e389cba6ff0d75664389eb2194d4bc48cfb15bd4dec89bab51e9"
        }


=== "Nexa"

    >
    >  Function: `blockchain.utxo.get(outpoint_hash)`
    >
    >  Version added: Rostrum 7.0
    >
    >  Update Rostrum 8.1: Add group details and scriptpukey
    >
    > *outpoint\_hash*
    >
    > > Hash of utxo (hash of transaction idem + output index)

    **Result**

    > A dictionary with the following keys:
    >
    > -  *status*
    >
    >    State of the utxo. A string that is "spent" or "unspent".
    >
    > -  *height*
    >
    >    The height the utxo was confirmed in. If it is unconfirmed, the
    >    value is 0 if all inputs are confirmed, and -1 otherwise.
    >
    > -  *value*
    >
    >    The output’s value in minimum coin units (satoshis).
    >
    > -  *scripthash*
    >
    >    The scriphash of the output scriptPubKey.
    >
    > -  *scriptpubkey*
    >
    >    The locking script of the utxo as hex.
    >
    > -  *spent*
    >
    >    The transaction spending the utxo with the following keys:
    >
    > >  -  *tx\_pos*
    > >
    > >    The zero-based index of the input in the transaction’s list of inputs. Null if utxo is unspent.
    > >
    > > -  *tx_hash*
    > >
    > >    The transaction ID. Null if utxo is unspent.
    > >
    > > -  *height*
    > >
    > >    The height the transaction was confirmed in. If it is unconfirmed, the
    > >    value is 0 if all inputs are confirmed, and -1 otherwise. Null if utxo is unspent.
    >
    > - *tx_idem*
    >
    >  The transaction idem of the utxo.
    >
    > - *tx_pos*
    >
    > The output index of the utxo.
    >
    > - *group*
    >
    > The group ID as cashaddr. Null if none.
    >
    > - *token_id_hex*
    >
    > The group ID as hex. Null if none.
    >
    > - *group_quantity*
    >
    > The amount of group in utxo. Null if none.
    >
    > - *template_scripthash*
    >
    > The template scripthash. Null if not template type utxo.
    >
    > - *template_argumenthash*
    >
    > The template argumenthash. Null if not template type utxo.

    **Example Results**

        {
            "addresses": [
                "nexareg:nqtsq5g5zshfe4kt90x9l7s43v5ts86l4gptwxw07rysgtdr"
            ],
            "amount": 546,
            "group": "nexareg:tqex0rdvw0fy7j4308j3k3sqpnnqy6j825cl4evmazeutql6ycqqqne3ew470",
            "group_authority": 0,
            "group_quantity": 42,
            "height": -1,
            "scripthash": "8cfc801e08cbb086a42f4b3c698b9b64c0a60cdc4b0af07d2742ac6b1828f547",
            "scriptpubkey": "2032678dac73d24f4ab179e51b46000ce6026a475531fae59be8b3c583fa260000022a005114142e9cd6cb2bcc5ffa158b28b81f5faa02b719cf",
            "spent": {
                "height": null,
                "tx_hash": null,
                "tx_pos": null
            },
            "status": "unspent",
            "template_argumenthash": "142e9cd6cb2bcc5ffa158b28b81f5faa02b719cf",
            "template_scripthash": "pay2pubkeytemplate",
            "token_id_hex": "32678dac73d24f4ab179e51b46000ce6026a475531fae59be8b3c583fa260000",
            "tx_hash": "25958055aa367ca4f16824a3c14d2fe6a7b297f4147316fdeac05a5a91354027",
            "tx_idem": "ed35b780d602e50ff601d7d1b24a1d6d88a937923eef4fb7591300d41718b49a",
            "tx_pos": 0
        }
