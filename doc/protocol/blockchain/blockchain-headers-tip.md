## blockchain.headers.tip

Get the latest block header (tip of the blockchain).

**Signature**

> Function: blockchain.headers.tip()
>
> Added: Rostrum 7.0

**Result**

> The header of the current block chain tip. The result is a dictionary
> with two members:
>
> -   *hex*
>
>     The binary header as a hexadecimal string.
>
> -   *height*
>
>     The height of the header, an integer.
>
> This call gives the same result as [blockchain.headers.subscribe](/protocol/blockchain/blockchain-headers-subscribe), but without starting to subscribe to headers.

**Example Result**

    {
      "height": 520481,
      "hex": "00000020890208a0ae3a3892aa047c5468725846577cfcd9b512b50000000000000000005dc2b02f2d297a9064ee103036c14d678f9afc7e3d9409cf53fd58b82e938e8ecbeca05a2d2103188ce804c4"
    }