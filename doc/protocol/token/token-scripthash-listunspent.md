## token.scripthash.listunspent

Return an list of token UTXOs sent to a script hash.

=== "Bitcoin Cash"

    **Signature**

    > Function:
    > token.scripthash.listunspent(scripthash, cursor *(optional)*, token *(optional)*)
    >
    > Version added:
    > Rostrum 6.0
    >
    > Updated in:
    > Rostrum 10.1 -- Added 'commitment' to result.
    >
    > -   *scripthash*
    >
    >     The script hash as a hexadecimal string.
    >
    > -   *cursor (optional)*
    >
    >     Null or string. If null, indicates that full list of unspent is requested.
    >
    >
    > -   *token (optional)*
    >
    > -   Token ID as hexadecimal string. If provided, will filter out transactions that do not include token.

    **Result**

    > A list of unspent token outputs. This function takes the
    > mempool into account. Mempool transactions paying to the address are
    > included at the in an undefined order. Any output that
    > is spent in the mempool does not appear.
    >
    > - *unspent*
    >
    >  List of each output in a dictionary with the following keys:
    >
    > > -   *height*
    > >
    > >    The integer height of the block the transaction was confirmed in.
    > >    `0` if the transaction is in the mempool.
    > >
    > > -   *tx_pos*
    > >
    > >    The zero-based index of the output in the transaction's list of
    > >    outputs.
    > >
    > > -   *tx_hash*
    > >
    > >    The output's transaction hash as a hexadecimal string. The hash
    > >    is little-endian encoded (same as bitcoind RPC).
    > >
    > > -   *value*
    > >
    > >    The output's value in minimum coin units (satoshis).
    > >
    > > -   *token_id*
    > >
    > >    The token identifier as a hexadecimal string (aka category)
    > >
    > > -   *token_amount*
    > >
    > >    The token amount in utxo
    > >
    > > -   *commitment*
    > >
    > >    If the output holds an commitment, the commitment as a hexadecimal string.
    > >
    >
    > -   *cursor*
    >
    >     Null if it was the last (or the full) result set. If there are more results to
    >     fetch, call the method again with this cursor value to get next set of results.

    **Result Example**


        {
          "unspent": [
            {
              "tx_pos": 0,
              "value": 546,
              "tx_hash": "9f2c45a12db0144909b5db269415f7319179105982ac70ed80d76ea79d923ebf",
              "height": 437146,
              "token_id": "5c087b9f1824d38c14e42d84c7bb9502da4a63b81cdb9b49e71bbe3b46c83111",
              "token_amount": 42,
              "commitment": "f0a3"
            },
            {
              "tx_pos": 0,
              "value": 546,
              "tx_hash": "3d2290c93436a3e964cfc2f0950174d8847b1fbe3946432c4784e168da0f019f",
              "height": 441696,
              "token_id": "8f6bda419cc2181a201731973aa3f965d79ea10da7bf3c23fd9b61dbf9e3dca7",
              "token_amount": 11,
            }
          ],
          "cursor": "201731973aa3f"
        }
=== "Nexa"

    **Signature**

    > Function:
    > token.scripthash.listunspent(scripthash, cursor *(optional)*, token *(optional)*)
    >
    > Version added:
    > Rostrum 6.0
    >
    > -   *scripthash*
    >
    >     The script hash as a hexadecimal string.
    >
    > -   *cursor (optional)*
    >
    >     Null or string. If null, indicates that full list of unspent is requested.
    >
    >
    > -   *token (optional)*
    >
    > -   Token ID as cashaddr encoded or hexadecimal string. If provided, will filter out transactions that do not include token.

    **Result**

    > A list of unspent token outputs. This function takes the
    > mempool into account. Mempool transactions paying to the address are
    > included at the in an undefined order. Any output that
    > is spent in the mempool does not appear.
    >
    > - *unspent*
    >
    >  List of each output in a dictionary with the following keys:
    >
    > > -   *height*
    > >
    > >    The integer height of the block the transaction was confirmed in.
    > >    `0` if the transaction is in the mempool.
    > >
    > > -   *tx_pos*
    > >
    > >    The zero-based index of the output in the transaction's list of
    > >    outputs.
    > >
    > > -   *tx_hash*
    > >
    > >    The output's transaction hash as a hexadecimal string. The hash
    > >    is little-endian encoded (same as bitcoind RPC).
    > >
    > > -   *value*
    > >
    > >    The output's value in minimum coin units (satoshis).
    > >
    > > -   *token_id_hex*
    > >
    > >    The token identifier as a hexadecimal string
    > >
    > > -   *group*
    > >
    > >    The token identifier as cashaddr
    > >
    > > -   *token_amount*
    > >
    > >    The token amount in utxo
    > >
    > > -   *outpoint_hash*
    > >
    > >    Hash of utxo (hash of transaction idem + output index)
    >
    > -   *cursor*
    >
    >     Null if it was the last (or the full) result set. If there are more results to
    >     fetch, call the method again with this cursor value to get next set of results.

    **Result Example**

        {
          "unspent": [
            {
              "tx_pos": 0,
              "value": 546,
              "tx_hash": "9f2c45a12db0144909b5db269415f7319179105982ac70ed80d76ea79d923ebf",
              "height": 437146,
              "token_id_hex": "5c087b9f1824d38c14e42d84c7bb9502da4a63b81cdb9b49e71bbe3b46c83111",
              "group": "nexareg:tzfl3s4qp4hzvjf2t6cy2eeyc67896pyflhlx6na382t2sapmyqqq3z44qjuc",
              "token_amount": 42,
              "outpoint_hash": "65a867e6d7da3f3986f8fe67fda311a60ac5c9b43e9f28326646f0d3f10381aa"
            },
            {
              "tx_pos": 0,
              "value": 546,
              "tx_hash": "3d2290c93436a3e964cfc2f0950174d8847b1fbe3946432c4784e168da0f019f",
              "height": 441696,
              "token_id_hex": "8f6bda419cc2181a201731973aa3f965d79ea10da7bf3c23fd9b61dbf9e3dca7",
              "group": "nexareg:tzfl3s4qp4hzvjf2t6cy2eeyc67896pyflhlx6na382t2sapmyqqq3z44qjuc",
              "token_amount": 11,
              "outpoint_hash": "2af3d1897865d2c4d265ba4c6da9720f29ed471653722a8093b56ab208bcfa42"
            }
          ],
          "cursor": "201731973aa3f"
        }


