## token.address.get\_mempool

Return the unconfirmed token transactions of a Nexa or Bitcoin Cash address.

**Signature**

> Function:
> token.address.get\_mempool(address, cursor *(optional)*, token *(optional)*)
>
> Version added:
> Rostrum 6.0
>
> -   *address*
>
>     The address as a Cash Address string (with or without prefix).
>     Legacy addresses (base58) are also supported.
>
> -   *cursor (optional)*
>
>     Null or string. If null, indicates that all histoy is requested. If a cursor
>     is provided from an earlier call, history is continued from last transaction in earlier call.
>
> -   *token (optional)*
>
>     Token ID as cashaddr encoded or hexadecimal string. If provided, will filter out transactions that do not include token.

**Result**

> As for `token.scripthash.get_mempool`