## token.transaction.get_history

=== "Bitcoin Cash"

    Return all confirmed and unconfirmed token transaction history of a given token.

    If no commitment is provided, full history, including all commitments (NFTs) is returned.
    To exclude all NFT's, pass empty string to commitment. Note that this will also exclude genesis if it mints NFT.

    **Signature**

    > Function:
    > token.transaction.get\_history(token, cursor *(optional)*, commitment *(optional)*)
    >
    > Version added:
    > Rostrum 6.0
    >
    > -   *token*
    >
    > -   Token ID as cashaddr encoded or hexadecimal string.
    >
    > -   *cursor (optional)*
    >
    >     Null or string. If null, indicates that all histoy is requested.
    >
    > -   *commitment (optional)*
    >
    >     Null or hexadecimal string. If specified, the history for the given NFT is fetched.

=== "Nexa"

    Return all confirmed and unconfirmed token transaction history of a given token.

    When a parent Token ID is passed, subgroup transactions are not included.

    **Signature**

    > Function:
    > token.transaction.get\_history(token, cursor *(optional)*)
    >
    > Version added:
    > Rostrum 6.0
    >
    > -   *token*
    >
    > -   Token ID as cashaddr encoded or hexadecimal string.
    >
    > -   *cursor (optional)*
    >
    >     Null or string. If null, indicates that all histoy is requested.


**Result**

> A list of unconfirmed and confirmed transactions. Unconfirmed transactions are returned first in
> no specific order. Confirmed transactions are returned by latest confirmed first.
>
> - *history*
>
>   List of transaction, dictionaries with the following keys:
>
> > -   *height*
> >
> >    The integer height of the block the transaction was confirmed in.
> >
> > -   *tx_hash*
> >
> >    The transaction hash in hexadecimal.
> >
>
> -   *cursor*
>
>     Null if it was the last (or the full) result set. If there are more results to
>     fetch, call the method again with this cursor value to get next set of results.

**Result Examples**

    {
      "history": [
        {
          "height": 0,
          "tx_hash": "9fbed79a1e970343fcd39f4a2d830a6bde6de0754ed2da70f489d0303ed558ec"
        },
        {
          "height": 200004,
          "tx_hash": "acc3758bd2a26f869fcc67d48ff30b96464d476bca82c1cd6656e7d506816412"
        },
        {
          "height": 215008,
          "tx_hash": "f3e1bf48975b8d6060a9de8884296abb80be618dc00ae3cb2f6cee3085e09403"
        },
      ],
      "cursor": "2012d830a6a3f"
    }
