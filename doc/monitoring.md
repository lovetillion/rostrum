# Monitoring

See [demo site](https://stats.bitcoincash.network).

Indexing and serving metrics are exported via [Prometheus](https://github.com/pingcap/rust-prometheus):

To monitor Bitcoin Cash mainnet:

```bash
$ sudo apt install prometheus
$ echo "
scrape_configs:
  - job_name: rostrum
    static_configs:
    - targets: ['localhost:4224']
" | sudo tee -a /etc/prometheus/prometheus.yml
$ sudo systemctl restart prometheus
$ firefox 'http://localhost:9090/graph?g0.range_input=1h&g0.expr=index_height&g0.tab=0'
```

For other networks, see [prometheus monitoring ports](ports.md).
