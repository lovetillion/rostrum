#!/usr/bin/env python3
# Copyright (c) 2022 The Bitcoin Unlimited developers
"""
Test the `token.transaction.get_history` RPC call
"""
import asyncio
from test_framework.util import assert_equal
from test_framework.electrumutil import (
    ElectrumTestFramework,
    get_txid_from_idem,
)
from test_framework.electrumconnection import ElectrumConnection
from test_framework.environment import on_nex, on_bch


class ElectrumTokenTransactionGetHistory(ElectrumTestFramework):
    def run_test(self):
        # This test users nexad wallet to create and send tokens.
        # Mine and mature some coins.
        n = self.nodes[0]
        n.generate(120)

        async def async_tests():
            cli = ElectrumConnection()
            await cli.connect()
            await self.sync_height(cli)
            try:
                await self.test_history(n, cli)
                await self.test_nft_history(n, cli)
            finally:
                cli.disconnect()

        asyncio.run(async_tests())

    async def test_history(self, n, cli):
        # This creates two transaction, creating tx + mint tx
        token_id = self.create_token(to_addr=n.getnewaddress(), mint_amount=42)

        # 8 more transactions for sending
        send_txids = []
        for _ in range(0, 8):
            txidem = self.send_token(token_id, n.getnewaddress(), 1)
            send_txids.append(get_txid_from_idem(n, txidem))

        await self.sync_mempool_count(cli)
        res = await cli.call("token.transaction.get_history", token_id)
        assert_equal(None, res["cursor"])
        assert_equal(10, len(res["history"]))

        for txid in send_txids:
            assert txid in map(lambda x: x["tx_hash"], res["history"])

        for item in res["history"]:
            assert_equal(0, item["height"])

        n.generate(1)
        await self.sync_all(cli)

        res = await cli.call("token.transaction.get_history", token_id)
        assert_equal(10, len(res["history"]))
        for item in res["history"]:
            assert_equal(n.getblockcount(), item["height"])

        # Create a tx confirmed in a later block, + 1 in mempool
        self.send_token(token_id, n.getnewaddress(), 1)
        n.generate(1)
        self.send_token(token_id, n.getnewaddress(), 1)
        await self.sync_all(cli)

        # The order returned should be:
        # [mempool, blockheight, blockheight - 1]
        h = (await cli.call("token.transaction.get_history", token_id))["history"]
        assert_equal(12, len(h))
        assert_equal(0, h[0]["height"])
        assert_equal(n.getblockcount(), h[1]["height"])
        assert_equal(n.getblockcount() - 1, h[2]["height"])

        # clean mempool
        n.generate(1)
        await self.wait_for_mempool_count(cli, count=0)

    async def test_nft_history(self, n, cli):
        """
        Check that nft transaction history not returns parent history
        """
        # On nexa, this creates two transaction, creating tx + mint tx
        # On bch, it creates and mints in same transaction
        assert_equal(0, (await cli.call("mempool.count"))["count"])
        token_id = self.create_token(
            to_addr=n.getnewaddress(), mint_amount=42, bch_can_mint_nft=True
        )

        self.create_nft(token_id, n.getnewaddress(), 255)
        self.create_nft(token_id, n.getnewaddress(), 24)

        async def check_parent_history(at_height: int):
            if on_nex():
                # Creation, mint, create nft, create nft
                if at_height == 0:
                    await self.wait_for_mempool_count(cli, count=4)
                res = await cli.call("token.transaction.get_history", token_id)
                assert_equal(4, len(res["history"]))
            if on_bch():
                # Creation+mint, create nft, create nft
                if at_height == 0:
                    await self.wait_for_mempool_count(cli, count=3)
                res = await cli.call("token.transaction.get_history", token_id)
                assert_equal(3, len(res["history"]))
            assert_equal(None, res["cursor"])
            for item in res["history"]:
                assert_equal(at_height, item["height"])

        await check_parent_history(0)

        if on_bch():
            # Edge case: This framework creates an NFT in the genesis
            # If commitment is filtered, then it won't appear in the result.
            commitment = ""
            res = await cli.call(
                "token.transaction.get_history", token_id, None, commitment
            )
            assert_equal(0, len(res["history"]))

        # Confirm in a block and check if confirmed yields the same.
        n.generate(1)
        await self.sync_all(cli)
        await check_parent_history(n.getblockcount())

        # Check history for a specific NFT
        nft = self.create_nft(token_id, n.getnewaddress(), 3)
        await self.sync_mempool_count(cli)

        async def check_nft_history(at_height: int):
            if on_nex():
                # NFT has unique token ID
                res = await cli.call("token.transaction.get_history", nft)
            if on_bch():
                # NFT has it's own "commitment", need to pass both
                res = await cli.call(
                    "token.transaction.get_history", token_id, None, nft
                )
            assert_equal(1, len(res["history"]))
            assert_equal(at_height, res["history"][0]["height"])

        await check_nft_history(0)

        n.generate(1)
        await self.sync_all(cli)

        await check_nft_history(n.getblockcount())


if __name__ == "__main__":
    ElectrumTokenTransactionGetHistory().main()
