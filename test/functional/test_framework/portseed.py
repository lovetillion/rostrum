import random

# The maximum number of nodes a single test can spawn
MAX_NODES = 8
# Don't assign rpc or p2p ports lower than this
PORT_MIN = 5000
# The number of ports to "reserve" for p2p and rpc, each
PORT_RANGE = 5000


class PortSeed:
    # Must be initialized with a unique integer for each process
    n = None

    # These map <node-n> to the ports assigned to it.
    ports = {
        "p2p": {},
        "rpc": {},
        "electrum_rpc": {},
        "electrum_ws": {},
        "electrum_monitoring": {},
    }

    # map node number to full initialized config file path for later fixup in case
    # the RPC or P2P port needs to be changed due to port collisions.
    config_file = {}

    def all_used_ports(self):
        """
        Get a list of all ports we've assigned to nodes.
        """
        used = []
        for service in self.ports.values():
            used.extend(service.values())
        return used

    def ports_as_string(self, n):
        """
        String representation of ports used by a single node.
        """
        ports_string = ""
        for service, port in self.ports.items():
            ports_string += f"{service}: {port[n]} "

        return ports_string

    def random_port(self):
        """
        Find a random port that we've not already assigned to a node.
        """
        p = random.randint(PORT_MIN, PORT_MIN + PORT_RANGE - 1)
        if p in self.all_used_ports():
            return self.random_port()
        return p


portseed = PortSeed()


def get_port(n, service):
    assert n <= MAX_NODES
    assert service in portseed.ports

    if n in portseed.ports[service]:
        return portseed.ports[service][n]

    seed_offset = (MAX_NODES * PortSeed.n) % (PORT_RANGE - 1 - MAX_NODES)

    service_index = list(portseed.ports).index(service)
    port = PORT_MIN + (service_index * PORT_RANGE) + n + seed_offset

    portseed.ports[service][n] = port
    return port


def p2p_port(n):
    return get_port(n, "p2p")


def rpc_port(n):
    return get_port(n, "rpc")


def electrum_rpc_port(n):
    return get_port(n, "electrum_rpc")


def electrum_ws_port(n):
    return get_port(n, "electrum_ws")


def electrum_monitoring_port(n):
    return get_port(n, "electrum_monitoring")
