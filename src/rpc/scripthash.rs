use std::sync::Arc;

use crate::chaindef::{BlockHash, ScriptHash};
use crate::errors::rpc_not_found;
use crate::indexes::scripthashindex::QueryFilter;
use crate::mempool::{Tracker, MEMPOOL_HEIGHT};
use crate::query::status::{
    by_block_height, confirmed_scripthash_balance, get_confirmed_outputs, scripthash_history,
    scripthash_listunspent, unconfirmed_history, unconfirmed_scripthash_balance,
};
use crate::query::Query;
use crate::rpc::encoding::blockhash_to_hex;
use crate::store::DBStore;
use anyhow::Result;
use bitcoin_hashes::hex::ToHex;
use bitcoin_hashes::Hash;
use rayon::slice::ParallelSliceMut;
use serde_json::Value;

pub async fn get_balance(
    query: &Query,
    scripthash: ScriptHash,
    filter: QueryFilter,
) -> Result<Value> {
    let confirmed = query.confirmed_index().clone();
    let (confirmed, confirmed_outputs) =
        confirmed_scripthash_balance(&confirmed, scripthash, filter).await?;

    let unconfirmed = query.unconfirmed_index().index().clone();
    let unconfirmed =
        unconfirmed_scripthash_balance(&unconfirmed, confirmed_outputs, scripthash, filter).await?;

    Ok(json!({
        "confirmed": confirmed,
        "unconfirmed": unconfirmed,
    }))
}

pub async fn get_first_use(
    query: &Query,
    scripthash: ScriptHash,
    filter: QueryFilter,
) -> Result<Value> {
    let firstuse = query.scripthash_first_use(scripthash, filter).await?;

    if firstuse.is_none() {
        return Err(rpc_not_found(format!(
            "scripthash '{}' not found",
            scripthash
        )));
    }
    let firstuse = firstuse.unwrap();

    let blockhash = if firstuse.0 == MEMPOOL_HEIGHT {
        BlockHash::all_zeros()
    } else {
        let h = query.get_headers(&[firstuse.0 as usize]).await;
        if h.is_empty() {
            warn!("expected to find header for height {}", firstuse.0);
            BlockHash::all_zeros()
        } else {
            h[0].block_hash()
        }
    };

    let height = if firstuse.0 == MEMPOOL_HEIGHT {
        0
    } else {
        firstuse.0
    };

    Ok(json!({
        "block_hash": blockhash_to_hex(&blockhash),
        "height": height,
        "block_height": height, // deprecated
        "tx_hash": firstuse.1.to_hex()
    }))
}

pub async fn get_history(
    store: &Arc<DBStore>,
    mempool: &Tracker,
    scripthash: ScriptHash,
    filter: QueryFilter,
) -> Result<Value> {
    Ok(json!(
        scripthash_history(store, mempool, scripthash, filter, None).await?
    ))
}

pub(crate) async fn get_mempool(
    query: &Query,
    scripthash: ScriptHash,
    filter: QueryFilter,
) -> Result<Value> {
    let confirmed_outputs =
        get_confirmed_outputs(query.confirmed_index(), scripthash, filter, None).await?;
    let mut history = unconfirmed_history(
        query.unconfirmed_index(),
        confirmed_outputs,
        scripthash,
        filter,
        None,
    )
    .await?;
    history.par_sort_by(by_block_height);
    Ok(json!(history))
}

pub async fn listunspent(
    query: &Query,
    scripthash: ScriptHash,
    filter: QueryFilter,
) -> Result<Value> {
    Ok(json!(
        scripthash_listunspent(
            query.confirmed_index(),
            query.unconfirmed_index().index(),
            scripthash,
            filter,
            None,
        )
        .await?
    ))
}
