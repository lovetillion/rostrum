use std::sync::Arc;

use serde_json::Value;

use crate::{errors::rpc_invalid_params, mempool::Tracker};

use super::parseutil::hex_from_value;
use anyhow::Result;

pub struct MempoolRPC {
    mempool: Arc<Tracker>,
}

impl MempoolRPC {
    pub fn new(mempool: Arc<Tracker>) -> Self {
        Self { mempool }
    }

    pub async fn mempool_get(&self, params: &[Value]) -> Result<Value> {
        let txs = if let Some(filter) = params.get(0) {
            if let Some(scriptsig) = filter.get("scriptsig") {
                let filter = hex_from_value(Some(scriptsig), "scriptsig")?;
                self.mempool
                    .get_txn_partially_matching_scriptsig(&filter)
                    .await
            } else {
                return Err(rpc_invalid_params("Unknown filter parameters".to_string()));
            }
        } else {
            // No filter, get full mempool
            self.mempool.get_all_txn().await
        };

        Ok(json!({
            "transactions": txs,
        }))
    }

    pub async fn mempool_count(&self) -> Result<Value> {
        Ok(json!({"count": self.mempool.get_count().await}))
    }

    pub(crate) async fn mempool_get_fee_histogram(&self) -> Value {
        json!(self.mempool.fee_histogram().await)
    }
}
