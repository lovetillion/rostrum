use crate::chaindef::{BlockHeader, ScriptHash};

pub mod communication;
pub mod connection;
pub mod server;
pub mod tcpcommunication;
pub mod wscommunication;

#[derive(Debug)]
pub enum Message {
    Request(String),
    ScriptHashChange(ScriptHash),
    ChainTipChange(Box<BlockHeader>),
    Done,
}

pub enum Notification {
    ScriptHashChange(ScriptHash),
    ChainTipChange(Box<BlockHeader>),
    Exit,
}
