use crate::chaindef::{BlockHeader, ScriptHash, Transaction};
use crate::encode::outpoint_hash;
use crate::rpc::daemon::connection::Connection;
use crate::rpc::daemon::tcpcommunication::TcpCommuncation;
use crate::rpc::daemon::wscommunication::WsCommunication;
use crate::signal::{NetworkNotifier, Waiter};
use crate::util::Channel;
use anyhow::Result;
use bitcoincash::hash_types::Txid;
use futures_util::{stream, StreamExt};
use std::collections::HashSet;
use std::net::SocketAddr;
use std::sync::Arc;
use tokio::io::AsyncWriteExt;
use tokio::net::{TcpListener, TcpStream};
use tokio::sync::mpsc::error::TrySendError;
use tokio::sync::mpsc::Sender;
use tokio::sync::Mutex;

use crate::chaindef::BlockHash;
use crate::config::Config;
use crate::def::PROTOCOL_VERSION_MAX;
use crate::doslimit::{ConnectionLimits, GlobalLimits};
use crate::metrics::Metrics;
use crate::query::Query;

use crate::metrics;

use crate::rpc::rpcstats::RpcStats;

use super::{Message, Notification};

#[cfg(not(feature = "nexa"))]
fn get_output_scripthash(txn: &Transaction, n: Option<usize>) -> Vec<ScriptHash> {
    if let Some(out) = n {
        vec![ScriptHash::from_script(&txn.output[out].script_pubkey)]
    } else {
        txn.output
            .iter()
            .map(|o| ScriptHash::from_script(&o.script_pubkey))
            .collect()
    }
}

#[cfg(feature = "nexa")]
fn get_output_scripthash(txn: &Transaction, n: Option<usize>) -> Vec<ScriptHash> {
    if let Some(out) = n {
        vec![match txn.output[out].scriptpubkey_without_token() {
            Ok(Some(s)) => ScriptHash::from_script(&s),
            _ => ScriptHash::from_script(&txn.output[out].script_pubkey),
        }]
    } else {
        txn.output
            .iter()
            .map(|o| match o.scriptpubkey_without_token() {
                Ok(Some(s)) => ScriptHash::from_script(&s),
                _ => ScriptHash::from_script(&o.script_pubkey),
            })
            .collect()
    }
}

pub struct Server {
    notification: tokio::sync::mpsc::Sender<Notification>,
    // so we can join the servers when dropping this object
    tcp_server: Option<tokio::task::JoinHandle<()>>,
    ws_server: Option<tokio::task::JoinHandle<()>>,
    query: Arc<Query>,
}

impl Server {
    async fn start_notifier(
        notification: Channel<Notification>,
        senders: Arc<Mutex<Vec<Sender<Message>>>>,
        acceptors: Vec<Sender<Option<(TcpStream, SocketAddr)>>>,
    ) {
        crate::thread::spawn_task("notification", async move {
            while let Some(msg) = notification.receiver().await.recv().await {
                let mut senders = senders.lock().await;
                match msg {
                    Notification::ScriptHashChange(hash) => senders.retain(|sender| {
                        match sender.try_send(Message::ScriptHashChange(hash)) {
                            Ok(_) => true,
                            Err(err) => match err {
                                TrySendError::Closed(_) => {
                                    trace!("scripthash notify failed: peer disconnected");
                                    false
                                }
                                TrySendError::Full(_) => {
                                    trace!("scripthash notify failed: peer channel full");
                                    false
                                }
                            },
                        }
                    }),
                    Notification::ChainTipChange(hash) => senders.retain(|sender| {
                        match sender.try_send(Message::ChainTipChange(hash.clone())) {
                            Ok(_) => true,
                            Err(err) => match err {
                                TrySendError::Closed(_) => {
                                    trace!("scripthash notify failed: peer disconnected");
                                    false
                                }
                                TrySendError::Full(_) => {
                                    trace!("scripthash notify failed: peer channel full");
                                    false
                                }
                            },
                        }
                    }),
                    // mark acceptor as done
                    Notification::Exit => {
                        for a in &acceptors {
                            trace!("peer exit notification");
                            let _ = a.send(None).await;
                        }
                    }
                }
            }
            Ok(())
        });
    }

    async fn start_acceptor(
        addr: SocketAddr,
        acceptor_type: String,
    ) -> Channel<Option<(TcpStream, SocketAddr)>> {
        let chan = Channel::bounded(100_000);
        let acceptor = chan.sender();
        crate::thread::spawn_task("acceptor", async move {
            let listener = match TcpListener::bind(addr).await {
                Ok(l) => l,
                Err(e) => {
                    let errmsg = format!(
                        "Failed to start {} server - bind({}) failed: {}",
                        acceptor_type, addr, e
                    );
                    Waiter::shutdown(&errmsg);
                    bail!(errmsg)
                }
            };

            info!(
                "Electrum {} RPC server running on {} (protocol {})",
                acceptor_type, addr, PROTOCOL_VERSION_MAX
            );
            loop {
                let (stream, addr) = listener.accept().await.expect("accept failed");

                match acceptor.send(Some((stream, addr))).await {
                    Ok(_) => {}
                    Err(e) => trace!("Failed to send to client {:?}", e),
                }

                if Waiter::shutdown_check().is_err() {
                    break;
                }
            }
            Ok(())
        });
        chan
    }

    pub async fn start(
        config: Arc<Config>,
        query: Arc<Query>,
        metrics: Arc<Metrics>,
        connection_limits: ConnectionLimits,
        global_limits: Arc<GlobalLimits>,
        network_notifier: Arc<NetworkNotifier>,
    ) -> Self {
        let stats = Arc::new(RpcStats {
            latency: metrics.histogram_vec(
                "rostrum_rpc_latency",
                "RPC latency (seconds)",
                &["method"],
                metrics::default_duration_buckets(),
            ),
            subscriptions: metrics.gauge_int(prometheus::Opts::new(
                "rostrum_scripthash_subscriptions",
                "# of scripthash subscriptions for node",
            )),
        });
        stats.subscriptions.set(0);

        let blockchain_event_queue = Channel::bounded(1_000_000);
        let blockchain_event_sender = blockchain_event_queue.sender();

        let tcp_acceptor = Self::start_acceptor(config.electrum_rpc_addr, "tcp".to_string()).await;
        let ws_acceptor =
            Self::start_acceptor(config.electrum_ws_addr, "websocket".to_string()).await;

        let rpc_queue_senders = Arc::new(Mutex::new(Vec::<Sender<Message>>::new()));

        Self::start_notifier(
            blockchain_event_queue,
            rpc_queue_senders.clone(),
            vec![tcp_acceptor.sender(), ws_acceptor.sender()],
        )
        .await;

        let limits_cpy = global_limits.clone();
        let config_cpy = config.clone();
        let query_cpy = query.clone();
        let stats_cpy = stats.clone();
        let senders_cpy = rpc_queue_senders.clone();
        let network_notifier_cpy = network_notifier.clone();
        let ws_server = crate::thread::spawn_task("ws_rpc", async move {
            while let Some(conn) = ws_acceptor.receiver().await.recv().await {
                let (mut stream, addr) = match conn {
                    Some(c) => c,
                    None => break,
                };

                let mut connections = match limits_cpy.inc_connection(&addr.ip()).await {
                    Err(e) => {
                        trace!("[{} ws] dropping peer - {}", addr, e);
                        let _ = stream.shutdown().await;
                        continue;
                    }
                    Ok(n) => n,
                };

                let config_cpy = config_cpy.clone();
                let query_cpy = query_cpy.clone();
                let stats_cpy = stats_cpy.clone();
                let limits_cpy = limits_cpy.clone();
                let network_notifier_cpy = network_notifier_cpy.clone();

                let senders_cpy = senders_cpy.clone();
                crate::thread::spawn_task("ws_peer", async move {
                    let rpc_queue: Channel<Message> = Channel::bounded(config_cpy.rpc_buffer_size);

                    senders_cpy.lock().await.push(rpc_queue.sender());

                    let com = match WsCommunication::init(stream).await {
                        Ok(c) => c,
                        Err(e) => {
                            // Initialization failed, but we already incremented connection. Need to decrement.
                            if let Err(e) = limits_cpy.dec_connection(&addr.ip()).await {
                                warn!("Error decrementing connection count: {}", e);
                            }
                            return Err(e);
                        }
                    };
                    debug!(
                        "[{} ws] connected peer ({:?} out of {:?} connection slots used)",
                        addr,
                        connections,
                        limits_cpy.connection_limits(),
                    );

                    let conn = Connection::new(
                        config_cpy,
                        query_cpy,
                        addr,
                        stats_cpy,
                        connection_limits,
                        Box::new(com),
                        network_notifier_cpy,
                    );
                    conn.run(rpc_queue).await;

                    match limits_cpy.dec_connection(&addr.ip()).await {
                        Ok(n) => connections = n,
                        Err(e) => error!("{}", e),
                    };
                    debug!(
                        "[{} ws] disconnected peer ({:?} out of {:?} connection slots used)",
                        addr,
                        connections,
                        limits_cpy.connection_limits(),
                    );

                    Ok(())
                });
            }
            Ok(())
        });

        // Create TCP server
        let senders_cpy = rpc_queue_senders.clone();
        let query_cpy = query.clone();
        let tcp_server = crate::thread::spawn_task("tcp_rpc", async move {
            while let Some(conn) = tcp_acceptor.receiver().await.recv().await {
                let (mut stream, addr) = match conn {
                    Some(c) => c,
                    None => break,
                };

                let global_limits = global_limits.clone();

                let mut connections = match global_limits.inc_connection(&addr.ip()).await {
                    Err(e) => {
                        trace!("[{}] dropping tcp peer - {}", addr, e);
                        let _ = stream.shutdown().await;
                        continue;
                    }
                    Ok(n) => n,
                };
                // explicitely scope the shadowed variables for the new thread
                let query_cpy = Arc::clone(&query_cpy);
                let stats_cpy = Arc::clone(&stats);
                let network_notifier_cpy = Arc::clone(&network_notifier);
                let config = Arc::clone(&config);
                let rpc_queue = Channel::bounded(config.rpc_buffer_size);

                senders_cpy.lock().await.push(rpc_queue.sender());

                crate::thread::spawn_task("tcp_peer", async move {
                    if connections != (0, 0) {
                        debug!(
                            "[{} tcp] connected peer ({:?} out of {:?} connection slots used)",
                            addr,
                            connections,
                            global_limits.connection_limits(),
                        );
                    }

                    let communication = TcpCommuncation::new(stream);

                    let conn = Connection::new(
                        config,
                        query_cpy,
                        addr,
                        stats_cpy,
                        connection_limits,
                        Box::new(communication),
                        network_notifier_cpy,
                    );
                    conn.run(rpc_queue).await;
                    match global_limits.dec_connection(&addr.ip()).await {
                        Ok(n) => connections = n,
                        Err(e) => error!("{}", e),
                    };
                    debug!(
                        "[{} tcp] disconnected peer ({:?} out of {:?} connection slots used)",
                        addr,
                        connections,
                        global_limits.connection_limits(),
                    );
                    Ok(())
                });
            }

            info!("RPC connections no longer accepted");
            Ok(())
        });

        Self {
            notification: blockchain_event_sender,
            query: query.clone(),
            tcp_server: Some(tcp_server),
            ws_server: Some(ws_server),
        }
    }

    /**
     * All scripthash subscriptions that should be notified. This includes all
     * scripthashes that received new coins (outputs of this transactions), and
     * all the scripthashes that had coins spent from them (inputs of this transaction).
     */
    async fn get_scripthashes_affected_by_tx(
        &self,
        txid: &Txid,
        blockhash: Option<&BlockHash>,
    ) -> Result<Vec<ScriptHash>> {
        let txn = self.query.tx().get(txid, blockhash, None, false).await?;
        let mut scripthashes = get_output_scripthash(&txn, None);

        for txin in txn.input {
            if txin.previous_output.is_null() {
                continue;
            }
            let tx = self
                .query
                .get_tx_funding_prevout(&outpoint_hash(&txin.previous_output))
                .await;
            if let Ok(Some((tx, index, _))) = tx {
                scripthashes.extend(get_output_scripthash(&tx, Some(index as usize)));
            }
        }
        Ok(scripthashes)
    }

    pub async fn notify_scripthash_subscriptions(
        &self,
        headers_changed: &[BlockHeader],
        txs_changed: HashSet<Txid>,
    ) {
        let mut txn_done: HashSet<Txid> = HashSet::new();
        let mut scripthashes: HashSet<ScriptHash> = HashSet::new();

        async fn insert_for_txs<'a>(
            parent: &Server,
            txids: &'a mut dyn Iterator<Item = Txid>,
            blockhash: Option<BlockHash>,
            txn_done: &HashSet<Txid>,
        ) -> (HashSet<Txid>, Vec<ScriptHash>) {
            let new: HashSet<_> = txids.collect();
            let new: HashSet<_> = new.difference(txn_done).cloned().collect();

            let hashes = stream::iter(&new)
                .map(|tx| async move {
                    let effected = parent
                        .get_scripthashes_affected_by_tx(tx, blockhash.as_ref())
                        .await;
                    let hashes = match effected {
                        Ok(hashes) => hashes,
                        Err(e) => {
                            trace!("failed to get effected scripthashes for tx {}: {:?}", tx, e);
                            vec![]
                        }
                    };
                    stream::iter(hashes.into_iter())
                })
                .buffer_unordered(10)
                .flatten()
                .collect::<Vec<ScriptHash>>()
                .await;

            (new, hashes)
        }

        for header in headers_changed.iter() {
            let blockhash = header.block_hash();
            let mut txids = match self.query.getblocktxids(&blockhash).await {
                Ok(txids) => txids.into_iter(),
                Err(e) => {
                    warn!("Failed to get blocktxids for {}: {}", blockhash, e);
                    continue;
                }
            };
            let (new_txs, new_scripthashes) =
                insert_for_txs(self, &mut txids, Some(blockhash), &txn_done).await;
            txn_done.extend(new_txs.iter());
            scripthashes.extend(new_scripthashes.iter());
        }

        let mut txs_changed_iter = txs_changed.into_iter();
        let (new_txs, new_scripthashes) =
            insert_for_txs(self, &mut txs_changed_iter, None, &txn_done).await;
        txn_done.extend(new_txs.iter());
        scripthashes.extend(new_scripthashes.iter());

        for s in scripthashes.drain() {
            if let Err(e) = self
                .notification
                .send(Notification::ScriptHashChange(s))
                .await
            {
                trace!("Scripthash change notification failed: {}", e);
            }
        }
    }

    pub async fn notify_subscriptions_chaintip(&self, header: BlockHeader) {
        if let Err(e) = self
            .notification
            .send(Notification::ChainTipChange(Box::new(header)))
            .await
        {
            trace!("Failed to notify about chaintip change {}", e);
        }
    }

    pub async fn disconnect_clients(&self) {
        trace!("disconncting clients");
        let _ = self.notification.send(Notification::Exit).await;
    }
}

impl Drop for Server {
    fn drop(&mut self) {
        trace!("stop accepting new RPCs");
        let n = self.notification.clone();
        crate::thread::spawn_task("drop_rpc", async move {
            let _ = n.send(Notification::Exit).await;
            Ok(())
        });
        if let Some(handle) = self.tcp_server.take() {
            handle.abort();
        }
        if let Some(handle) = self.ws_server.take() {
            handle.abort();
        }
        trace!("RPC server is stopped");
    }
}
