use std::time::Duration;

use anyhow::{Context, Result};
use async_trait::async_trait;
use serde_json::Value;
use tokio::{
    io::{AsyncBufReadExt, AsyncWriteExt, BufReader},
    net::{
        tcp::{OwnedReadHalf, OwnedWriteHalf},
        TcpStream,
    },
    sync::mpsc::Sender,
    time::timeout,
};

use crate::signal::Waiter;

use super::{communication::Communcation, Message};

pub struct TcpCommuncation {
    reader: Option<OwnedReadHalf>,
    writer: OwnedWriteHalf,
}

impl TcpCommuncation {
    pub fn new(stream: TcpStream) -> Self {
        let (reader, writer) = stream.into_split();

        Self {
            reader: Some(reader),
            writer,
        }
    }

    async fn parse_requests(
        mut reader: BufReader<OwnedReadHalf>,
        tx: Sender<Message>,
        idle_timeout: u64,
    ) -> Result<()> {
        loop {
            if Waiter::shutdown_check().is_err() {
                let _ = tx.send(Message::Done).await;
                bail!("shutdown");
            }
            let mut line = Vec::<u8>::new();

            // Setting a timeout for the read_until operation
            let read_result = timeout(
                Duration::from_secs(idle_timeout),
                reader.read_until(b'\n', &mut line),
            )
            .await;

            match read_result {
                Ok(Ok(bytes_read)) => {
                    if bytes_read == 0 {
                        tx.send(Message::Done).await.context("channel closed")?;
                        return Ok(());
                    }
                    if line.starts_with(&[22, 3, 1]) {
                        // (very) naive SSL handshake detection
                        let _ = tx.send(Message::Done).await;
                        bail!("invalid request - maybe SSL-encrypted data?: {:?}", line)
                    }
                    match String::from_utf8(line) {
                        Ok(req) => tx
                            .send(Message::Request(req))
                            .await
                            .context("channel closed")?,
                        Err(err) => {
                            let _ = tx.send(Message::Done).await;
                            bail!("invalid UTF8: {}", err)
                        }
                    }
                }
                // Handling the timeout case
                Err(tokio::time::error::Elapsed { .. }) => {
                    let _ = tx.send(Message::Done).await;
                    bail!(
                        "idle client did not send a message for {} seconds",
                        idle_timeout
                    );
                }
                Ok(Err(e)) => {
                    bail!("failed to read a request: {}", e);
                }
            }
        }
    }
}

#[async_trait]
impl Communcation for TcpCommuncation {
    async fn send_values(&mut self, values: &[Value]) -> Result<()> {
        for value in values {
            let line = value.to_string() + "\n";
            if let Err(e) = self.writer.write_all(line.as_bytes()).await {
                let truncated: String = line.chars().take(80).collect();
                return Err(e).context(format!("failed to send {}", truncated));
            }
        }
        Ok(())
    }

    fn start_request_receiver(&mut self, rpc_queue_sender: Sender<Message>, idle_timeout: u64) {
        let reader = self.reader.take();

        crate::thread::spawn_task("peer_reader", async move {
            // Give the reader to this task
            let bufreader = BufReader::new(reader.unwrap());
            if let Err(e) = Self::parse_requests(bufreader, rpc_queue_sender, idle_timeout).await {
                trace!("peer_reader thread: {}", e);
            }
            Ok(())
        });
    }

    async fn shutdown(&mut self) {
        let _ = self.writer.shutdown().await;
    }
}
