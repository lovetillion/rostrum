use std::time::Duration;

use anyhow::Result;
use async_trait::async_trait;
use futures_util::stream::{SplitSink, SplitStream, StreamExt};
use futures_util::SinkExt;
use serde_json::Value;
use tokio::net::TcpStream;
use tokio::sync::mpsc::Sender;
use tokio::time::timeout;
use tokio_tungstenite::tungstenite::Message as TokioMessage;
use tokio_tungstenite::WebSocketStream;

use crate::signal::Waiter;

use super::communication::Communcation;
use super::Message;

pub struct WsCommunication {
    writer: SplitSink<WebSocketStream<TcpStream>, TokioMessage>,
    reader: Option<SplitStream<WebSocketStream<TcpStream>>>,
}

impl WsCommunication {
    pub async fn init(stream: TcpStream) -> Result<Self> {
        let ws = tokio_tungstenite::accept_async(stream).await?;
        let (writer, reader) = ws.split();

        Ok(Self {
            reader: Some(reader),
            writer,
        })
    }
}

#[async_trait]
impl Communcation for WsCommunication {
    async fn send_values(&mut self, values: &[Value]) -> Result<()> {
        for value in values {
            let line = value.to_string();
            self.writer.feed(TokioMessage::Text(line)).await?;
        }
        self.writer.flush().await?;
        Ok(())
    }

    fn start_request_receiver(&mut self, rpc_queue_sender: Sender<Message>, idle_timeout: u64) {
        let reader = self.reader.take();

        crate::thread::spawn_task("ws_peer_reader", async move {
            // Give the reader to this task
            let mut reader = reader.unwrap();

            loop {
                Waiter::shutdown_check()?;
                let msg = timeout(Duration::from_secs(idle_timeout), reader.next()).await;

                let msg = match msg {
                    Ok(Some(message)) => message,
                    Ok(None) => break, // Connection closed
                    Err(_) => {
                        let _ = rpc_queue_sender.send(Message::Done).await;
                        return Err(anyhow!("Idle timeout {} exceeded", idle_timeout));
                    }
                };
                let msg = match msg {
                    Ok(msg) => msg,
                    Err(err) => {
                        trace!("ws peer error: {}", err.to_string());
                        let _ = rpc_queue_sender.send(Message::Done).await;
                        break;
                    }
                };

                if msg.is_text() {
                    let request = match msg.into_text() {
                        Ok(r) => r,
                        Err(e) => {
                            trace!("ws peer parse error: {}", e);
                            let _ = rpc_queue_sender.send(Message::Done).await;
                            break;
                        }
                    };
                    rpc_queue_sender.send(Message::Request(request)).await?;
                } else if msg.is_binary() {
                    trace!("Ignoring binary message from ws peer");
                } else if msg.is_close() {
                    trace!("ws close event received");
                    let _ = rpc_queue_sender.send(Message::Done).await;
                    break;
                }
            }

            Ok(())
        });
    }

    async fn shutdown(&mut self) {
        let _ = self.writer.close().await;
    }
}
