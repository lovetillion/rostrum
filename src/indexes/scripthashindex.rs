use bitcoin_hashes::Hash;

use crate::chaindef::{OutPointHash, ScriptHash};
use crate::store::Row;

use super::DBRow;

const SCRIPTHASHINDEX_CF: &str = "scripthash";

/**
 * A flag (1 byte) to define type of output to allow efficent
 * filtering.
 */
#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub enum OutputFlags {
    /// "Normal" output.
    None = 0,
    /// Output contains token in it.
    HasTokens = 1,
}

/**
 * Query filter
 */
#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub enum QueryFilter {
    /// Include all outputs
    IncludeAll,
    /// Include only outputs with tokens in them
    TokenOnly,
    /// Include only outputs that do not contain tokens
    TokenExcluded,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct ScriptHashIndexKey {
    pub scripthash: [u8; 32],
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct ScriptHashIndexRow {
    key: ScriptHashIndexKey,
    flags: u8,
    outpointhash: [u8; 32],
}

impl ScriptHashIndexRow {
    pub fn new(
        scripthash: &ScriptHash,
        outpointhash: &OutPointHash,
        flags: OutputFlags,
    ) -> ScriptHashIndexRow {
        ScriptHashIndexRow {
            key: ScriptHashIndexKey {
                scripthash: scripthash.into_inner(),
            },
            flags: flags as u8,
            outpointhash: outpointhash.into_inner(),
        }
    }

    fn filter_include_all(scripthash: [u8; 32]) -> Vec<u8> {
        bincode::serialize(&ScriptHashIndexKey { scripthash }).unwrap()
    }

    pub fn filter_by_scripthash(scripthash: [u8; 32], filter: &QueryFilter) -> Vec<u8> {
        match filter {
            QueryFilter::IncludeAll => Self::filter_include_all(scripthash),
            QueryFilter::TokenOnly => Self::filter_include_all(scripthash)
                .into_iter()
                .chain(std::iter::once(OutputFlags::HasTokens as u8))
                .collect(),
            QueryFilter::TokenExcluded => Self::filter_include_all(scripthash)
                .into_iter()
                .chain(std::iter::once(OutputFlags::None as u8))
                .collect(),
        }
    }

    pub fn outpointhash(&self) -> OutPointHash {
        OutPointHash::from_inner(self.outpointhash)
    }

    pub fn has_token(&self) -> bool {
        self.flags == OutputFlags::HasTokens as u8
    }
}

impl DBRow for ScriptHashIndexRow {
    const CF: &'static str = SCRIPTHASHINDEX_CF;

    fn to_row(&self) -> Row {
        Row {
            key: bincode::serialize(&self).unwrap().into_boxed_slice(),
            value: vec![].into_boxed_slice(),
        }
    }

    fn from_row(row: &Row) -> ScriptHashIndexRow {
        bincode::deserialize(&row.key).expect("failed to read ScriptHashIndexRow")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_to_from_row() {
        let row = ScriptHashIndexRow::new(
            &ScriptHash::hash(&[42]),
            &OutPointHash::hash(&[11]),
            OutputFlags::HasTokens,
        );
        let decoded_row = ScriptHashIndexRow::from_row(&row.to_row());
        assert_eq!(row, decoded_row);
    }

    #[test]
    fn test_filter_by_scripthash_with_token() {
        let scripthash = ScriptHash::hash(&[42]);
        let row_with_token = ScriptHashIndexRow::new(
            &scripthash,
            &OutPointHash::hash(&[11]),
            OutputFlags::HasTokens,
        )
        .to_row();
        let row_without_token =
            ScriptHashIndexRow::new(&scripthash, &OutPointHash::hash(&[11]), OutputFlags::None)
                .to_row();

        let filter_token = ScriptHashIndexRow::filter_by_scripthash(
            scripthash.into_inner(),
            &QueryFilter::TokenOnly,
        );
        let all = ScriptHashIndexRow::filter_by_scripthash(
            scripthash.into_inner(),
            &QueryFilter::IncludeAll,
        );
        let exclude_token = ScriptHashIndexRow::filter_by_scripthash(
            scripthash.into_inner(),
            &QueryFilter::TokenExcluded,
        );

        let starts_with = |a: &Row, b: &Vec<u8>| -> bool {
            a.key.to_vec().iter().zip(b.iter()).all(|(x, y)| x == y)
        };
        assert!(starts_with(&row_with_token, &filter_token));
        assert!(!starts_with(&row_without_token, &filter_token));

        assert!(starts_with(&row_with_token, &all));
        assert!(starts_with(&row_without_token, &all));

        assert!(!starts_with(&row_with_token, &exclude_token));
        assert!(starts_with(&row_without_token, &exclude_token));
    }
}
