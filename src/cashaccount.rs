use crate::chaindef::Transaction;
use crate::indexes::cashaccount::TxCashAccountRow;
use crate::indexes::DBRow;
use crate::mempool::MEMPOOL_HEIGHT;
use crate::store::DBStore;
use crate::utilscript::read_push_from_script;
use crate::writebatch::WriteBatch;
use anyhow::Result;
use bitcoincash::blockdata::script::Script;
use bitcoincash::hash_types::Txid;
use futures::stream::StreamExt;

pub async fn txids_by_cashaccount<'a>(
    store: &'a DBStore,
    name: &str,
    height: u32,
) -> (
    tokio::task::JoinHandle<Result<()>>,
    impl futures::stream::Stream<Item = Txid> + 'a,
) {
    let (task, stream) = store
        .scan(
            TxCashAccountRow::CF,
            TxCashAccountRow::filter(name.to_ascii_lowercase().as_bytes(), height),
        )
        .await;
    (
        task,
        stream.map(|row| TxCashAccountRow::from_row(&row).txid()),
    )
}

const CASHACCOUNT_PREFIX: [u8; 6] = [
    0x6a, /* op return */
    0x04, /* push 4 bytes */
    0x01, 0x01, 0x01, 0x01,
];

fn is_valid_cashaccount_name(name: &str) -> bool {
    if name.is_empty() || name.len() > 99 {
        return false;
    }

    for char in name.chars() {
        match char {
            'a'..='z' | 'A'..='Z' | '0'..='9' | '_' => {}
            _ => return false,
        }
    }

    true
}

fn parse_account_from_opreturn(script: &Script) -> Option<String> {
    if !script.as_bytes().starts_with(&CASHACCOUNT_PREFIX) {
        return None;
    }
    let mut iter = script.as_bytes()[CASHACCOUNT_PREFIX.len()..].iter();
    // `iter` needs to be borrowed in `read_push_data_len`, so we have to use `while let` instead
    // of `for`.

    let mut parts: Vec<Vec<u8>> = vec![];
    loop {
        let result = read_push_from_script(iter);
        if result.is_err() {
            // Invalid push operation in op return makes it an invalid cash account.
            return None;
        }
        let part;
        (iter, part) = result.unwrap();
        if let Some(p) = part {
            parts.push(p);
        } else {
            break;
        }
    }

    if parts.len() < 2 {
        // Too few parts (no payload)
        return None;
    }

    let cashaccount_name = match String::from_utf8(parts.first()?.to_vec()) {
        Ok(s) => s,
        Err(_) => return None,
    };

    if !is_valid_cashaccount_name(&cashaccount_name) {
        return None;
    }

    // We don't index the payloads, so going to ignore the rest of the parts.
    // We know that they were valid pushes. The specification states we should ignore
    // stuff we don't understand for forward compatibility, so it's safe to ignore.

    Some(cashaccount_name)
}

pub fn parse_cashaccount(txn: &Transaction) -> Option<String> {
    let mut opreturn_found = false;
    let mut cashaccount: Option<String> = None;
    for out in txn.output.iter() {
        if !out.script_pubkey.is_op_return() {
            continue;
        }
        if opreturn_found {
            // CashAccount transaction can only contain 1 OP_RETURN output.
            // We've now seen a second one.
            return None;
        }

        // OP_RETURN found. Parse to see if it contains a cashaccount.
        opreturn_found = true;
        cashaccount = parse_account_from_opreturn(&out.script_pubkey);
    }
    cashaccount
}

pub const CASHACCOUNT_INDEX_DISABLED: u32 = 0;

pub fn is_valid_cashaccount_height(activation_height: u32, height: u32) -> bool {
    height >= activation_height && height != MEMPOOL_HEIGHT && height != CASHACCOUNT_INDEX_DISABLED
}

pub struct CashAccountParser {
    activation_height: u32,
}

impl CashAccountParser {
    pub fn new(activation_height: Option<u32>) -> CashAccountParser {
        CashAccountParser {
            activation_height: activation_height.unwrap_or(0),
        }
    }

    pub fn has_cashaccount(&self, txn: &Transaction, name: &str) -> bool {
        match parse_cashaccount(txn) {
            Some(n) => name.to_ascii_lowercase() == n.to_ascii_lowercase(),
            _ => false,
        }
    }

    pub fn index_cashaccount(&self, writebatch: &WriteBatch, txn: &Transaction, blockheight: u32) {
        if !is_valid_cashaccount_height(self.activation_height, blockheight) {
            return;
        }
        let name = match parse_cashaccount(txn) {
            Some(n) => n,
            None => return,
        };
        writebatch.insert(
            TxCashAccountRow::CF,
            rayon::iter::once(
                TxCashAccountRow::new(
                    &txn.txid(),
                    name.to_ascii_lowercase().as_bytes(),
                    blockheight,
                )
                .to_row(),
            ),
        );
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_is_valid_cashaccount_height() {
        let activation = 1000;
        assert!(!is_valid_cashaccount_height(
            activation,
            CASHACCOUNT_INDEX_DISABLED
        ));
        assert!(!is_valid_cashaccount_height(activation, MEMPOOL_HEIGHT));
        assert!(is_valid_cashaccount_height(activation, MEMPOOL_HEIGHT - 1));

        assert!(!is_valid_cashaccount_height(activation, activation - 1));
        assert!(is_valid_cashaccount_height(activation, activation));
        assert!(is_valid_cashaccount_height(activation, activation + 1));
    }

    /// OP_RETURN <OP_PUSH(4)><protocol> <OP_PUSH(8)><account_name>
    /// OP_PUSH(21=0x15)><payment_data>
    #[test]
    fn opreturn_test1() {
        let case1 = hex::decode(
            "6a0401010101084a6f6e617468616e1501ebdeb6430f3d16a9c6758d6c0d7a400c8e6bbee4",
        )
        .unwrap();
        let name = parse_account_from_opreturn(&Script::from(case1)).unwrap();
        assert_eq!("Jonathan", name);
    }

    /// OP_RETURN <OP_PUSH(4)><protocol> <OP_PUSHDATA1><99=0x63><account_name>
    /// OP_PUSH(21=0x15)><payment_data>
    #[test]
    fn opreturn_test2() {
        let case2 = hex::decode(
        "6a04010101014c63495f616d5f796f75725f6b696e675f57656c6c5f495f6469646e745f766f74655f666f725f796f755f596f755f646f6e745f766f74655f666f725f6b696e67735f57656c6c5f686f775f6469645f796f755f6265636f6d655f6b696e675f7468656e5f15014685ab7ecdcee6addf8928cf684b47b07b0aa8f6").unwrap();
        let name = parse_account_from_opreturn(&Script::from(case2)).unwrap();
        assert_eq!("I_am_your_king_Well_I_didnt_vote_for_you_You_dont_vote_for_kings_Well_how_did_you_become_king_then_",
                      name);
    }

    #[test]
    fn opreturn_test3() {
        let case3 = hex::decode(
        "6a04010101010c4d6f6e7374657262697461724c5103010003c70482cfa903349d641659cf407e9421d0569788bec2154910a6a5914e6c89b0992a77fedb78855f9c6c22ca070904577f7cab40910e63cf9cc18492e73071a60000000000000000000000000015018d4e356539eaac7d84b1216030bd9f12424bbb72").unwrap();
        let name = parse_account_from_opreturn(&Script::from(case3)).unwrap();
        assert_eq!("Monsterbitar", name);
    }
}
