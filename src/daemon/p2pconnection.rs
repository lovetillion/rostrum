use anyhow::{Context, Result};
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::TcpStream,
    select,
};

use crate::{
    chaindef::net_magic,
    def::ROSTRUM_VERSION,
    errors::ConnectionError,
    metrics,
    signal::{NetworkNotifier, Waiter},
    util::Channel,
};
use bitcoincash::{
    consensus::{
        encode::{self, ReadExt, VarInt},
        Decodable,
    },
    network::{
        address,
        message::{self, CommandString, NetworkMessage},
        message_blockdata::Inventory,
        message_network,
    },
    Block, Network, Txid,
};
use rand::prelude::*;

use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::sync::Arc;
use std::time::{Duration, Instant, SystemTime, UNIX_EPOCH};
use std::{
    collections::HashSet,
    io::{self, ErrorKind},
    sync::atomic::AtomicBool,
    sync::atomic::Ordering,
};

use prometheus::HistogramVec;

pub struct P2PConnection {
    is_broken: Arc<AtomicBool>,
}

impl P2PConnection {
    pub(crate) async fn connect(
        network: Network,
        address: SocketAddr,
        network_signals: Arc<NetworkNotifier>,
        send_duration: Arc<HistogramVec>,
        recv_duration: Arc<HistogramVec>,
        parse_duration: Arc<HistogramVec>,
        recv_size: Arc<HistogramVec>,
    ) -> Result<Self, ConnectionError> {
        let conn = match TcpStream::connect(address).await {
            Ok(c) => c,
            Err(err) => {
                return Err(ConnectionError {
                    msg: format!("p2p failed to connect to {:?} {}", address, err),
                })
            }
        };
        let (mut stream_read, mut stream_write) = conn.into_split();

        let netmsg_channel = Channel::<NetworkMessage>::bounded(1);
        let netmsg_sender = netmsg_channel.sender();
        let rawmsg_channel = Channel::<RawNetworkMessage>::bounded(1);
        let rawmsg_sender = rawmsg_channel.sender();

        let is_broken: Arc<AtomicBool> = Arc::new(AtomicBool::new(false));
        let is_broken_copy = Arc::clone(&is_broken);

        crate::thread::spawn_task("p2p_send", async move {
            loop {
                let mut netmsg_recv = netmsg_channel.receiver().await;
                Waiter::shutdown_check()?;
                let start = Instant::now();
                let msg = match netmsg_recv.recv().await {
                    Some(msg) => msg,
                    None => {
                        is_broken_copy.store(true, Ordering::Relaxed);
                        return Ok(());
                    }
                };

                metrics::observe(&send_duration, "wait", duration_to_seconds(start.elapsed()));
                if let Err(e) = {
                    let start = Instant::now();
                    trace!("send: {:?}", msg);
                    let raw_msg = message::RawNetworkMessage {
                        magic: net_magic(network),
                        payload: msg,
                    };
                    let res = stream_write
                        .write_all(encode::serialize(&raw_msg).as_slice())
                        .await
                        .context("p2p failed to send");

                    let duration = duration_to_seconds(start.elapsed());
                    metrics::observe(&send_duration, "send", duration);
                    res
                } {
                    is_broken_copy.store(true, Ordering::Relaxed);
                    let _ = stream_write.shutdown().await;
                    return Err(e);
                }
            }
        });

        let is_broken_copy = Arc::clone(&is_broken);
        crate::thread::spawn_task("p2p_recv", async move {
            loop {
                Waiter::shutdown_check()?;
                let start = Instant::now();
                let raw_msg = RawNetworkMessage::async_consensus_decode(&mut stream_read).await;
                {
                    let duration = duration_to_seconds(start.elapsed());
                    let label = format!(
                        "recv_{}",
                        raw_msg
                            .as_ref()
                            .map(|msg| msg.cmd.as_ref())
                            .unwrap_or("err")
                    );
                    metrics::observe(&recv_duration, &label, duration);
                }
                let raw_msg = match raw_msg {
                    Ok(raw_msg) => {
                        metrics::observe(
                            &recv_size,
                            raw_msg.cmd.as_ref(),
                            raw_msg.raw.len() as f64,
                        );
                        if raw_msg.magic != net_magic(network) {
                            return Err(anyhow!(
                                "unexpected magic {} (instead of {})",
                                raw_msg.magic,
                                net_magic(network)
                            ));
                        }
                        raw_msg
                    }
                    Err(encode::Error::Io(e)) if e.kind() == ErrorKind::UnexpectedEof => {
                        debug!("closing p2p_recv thread: connection closed");
                        is_broken_copy.store(true, Ordering::Relaxed);
                        return Ok(());
                    }
                    Err(e) => {
                        debug!("failed to recv a message from peer: {}", e);
                        is_broken_copy.store(true, Ordering::Relaxed);
                        return Ok(());
                    }
                };

                let start = Instant::now();
                rawmsg_sender.send(raw_msg).await?;
                metrics::observe(&recv_duration, "send", duration_to_seconds(start.elapsed()));
            }
        });

        let init_channel = Channel::<()>::bounded(1);
        let init_channel_sender = init_channel.sender();

        match netmsg_sender.send(build_version_message()).await {
            Ok(_) => {}
            Err(err) => {
                is_broken.store(true, Ordering::Relaxed);
                return Err(ConnectionError {
                    msg: format!("p2p failed to send msg: {}", err),
                });
            }
        }

        let is_broken_copy = Arc::clone(&is_broken);
        crate::thread::spawn_task("p2p_loop", async move {
            loop {
                Waiter::shutdown_check()?;
                let block_sender = network_signals.block_channel.sender();
                let tx_sender = network_signals.tx_channel.sender();

                let mut rawmsg_recv = rawmsg_channel.receiver().await;
                select! {
                    raw_msg = rawmsg_recv.recv() => {
                        let raw_msg = match raw_msg {
                            Some(r) => r,
                            None => {
                                is_broken_copy.store(true, Ordering::Relaxed);
                                return Ok(())
                                                    }
                        };
                        let label = format!("parse_{}", raw_msg.cmd.as_ref());
                        let msg = match metrics::observe_duration(&parse_duration, &label, || raw_msg.parse()) {
                            Ok(msg) => msg,
                            Err(err) => {
                                is_broken_copy.store(true, Ordering::Relaxed);
                                debug!("failed to parse '{}({:?})': {}", raw_msg.cmd, raw_msg.raw, err);
                                return Ok(());
                            }
                        };

                        match msg {
                            NetworkMessage::GetHeaders(_) => {
                                match netmsg_sender.send(NetworkMessage::Headers(vec![])).await {
                                    Ok(_) => {},
                                    Err(err) => {
                                        is_broken_copy.store(true, Ordering::Relaxed);
                                        debug!("failed to reply to getheaders: {}", err);
                                        return Ok(())
                                    }
                                }
                            }
                            NetworkMessage::Version(version) => {
                                debug!("peer version: {:?}", version);
                                match netmsg_sender.send(NetworkMessage::Verack).await {
                                    Ok(_) => {},
                                    Err(err) => {
                                        is_broken_copy.store(true, Ordering::Relaxed);
                                        debug!("failed to reply to version: {}", err);
                                        return Ok(())
                                    }
                                }
                            }
                            NetworkMessage::Inv(inventory) => {
                                let mut new_txs: HashSet<Txid> = HashSet::new();

                                for inv in inventory {
                                    match inv {
                                        Inventory::Transaction(txid) => {
                                            new_txs.insert(txid);
                                        }
                                        Inventory::Block(blockhash) | Inventory::CompactBlock(blockhash) => {
                                            // best-effort notification
                                            #[cfg(feature = "nexa")]
                                            {
                                                use bitcoin_hashes::Hash;
                                                // TODO: Fix this hash conversion hack
                                                let blockhash = crate::nexa::hash_types::BlockHash::from_inner(blockhash.into_inner());
                                                if let Err(e) = block_sender.try_send(blockhash) {
                                                    trace!("Failed to notify of block: {}", e);
                                                }
                                            }
                                            #[cfg(not(feature = "nexa"))]
                                            {
                                                let _ = block_sender.try_send(blockhash);
                                            }
                                        }
                                        _ => { /* ignore */}
                                    }
                                }
                                // best effort
                                if let Err(e) = tx_sender.try_send(new_txs) {
                                    trace!("Failed to notify of inv: {}", e);
                                }

                            },
                            NetworkMessage::Ping(nonce) => {
                                // connection keep-alive
                                match netmsg_sender.send(NetworkMessage::Pong(nonce)).await {
                                    Ok(_) => {},
                                    Err(err) => {
                                        is_broken_copy.store(true, Ordering::Relaxed);
                                        debug!("Failed to respond to ping: {}", err);
                                        return Ok(())
                                    }
                                };
                            }
                            NetworkMessage::Verack => {
                                 // peer acknowledged our version
                                match init_channel_sender.send(()).await {
                                    Ok(_) => {},
                                    Err(err) => {
                                        is_broken_copy.store(true, Ordering::Relaxed);
                                        return Err(err.into());
                                    }
                                };
                            }
                            NetworkMessage::Block(_) => (),
                            NetworkMessage::Headers(_) => (),
                            NetworkMessage::Alert(_) => (),  // https://bitcoin.org/en/alert/2016-11-01-alert-retirement
                            NetworkMessage::Addr(_) => (),   // unused
                            msg => debug!("unexpected message: {:?}", msg),
                        }
                    }
                }
            }
        });

        // wait until `verack` is received
        if init_channel.receiver().await.recv().await.is_none() {
            is_broken.store(true, Ordering::Relaxed);
            return Err(ConnectionError {
                msg: "p2p failed to receive verack".to_string(),
            });
        }

        Ok(P2PConnection { is_broken })
    }

    pub fn is_broken(&self) -> bool {
        Waiter::shutdown_check().is_err() || self.is_broken.load(Ordering::Relaxed)
    }
}

fn build_version_message() -> NetworkMessage {
    let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)), 0);
    let timestamp = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time error")
        .as_secs() as i64;

    let services = bitcoincash::network::constants::ServiceFlags::NONE;

    #[cfg(not(feature = "nexa"))]
    let version = bitcoincash::network::constants::PROTOCOL_VERSION;
    #[cfg(feature = "nexa")]
    let version = 80003;

    NetworkMessage::Version(message_network::VersionMessage {
        version,
        services,
        timestamp,
        receiver: address::Address::new(&addr, services),
        sender: address::Address::new(&addr, services),
        nonce: rand::thread_rng().gen(),
        user_agent: format!("/rostrum:{}/", ROSTRUM_VERSION),
        start_height: 0,
        relay: true,
    })
}

struct RawNetworkMessage {
    magic: u32,
    cmd: CommandString,
    raw: Vec<u8>,
}

impl RawNetworkMessage {
    fn parse(&self) -> Result<NetworkMessage> {
        let mut raw: &[u8] = &self.raw;
        let payload = match self.cmd.as_ref() {
            "version" => NetworkMessage::Version(Decodable::consensus_decode(&mut raw)?),
            "verack" => NetworkMessage::Verack,
            "inv" => NetworkMessage::Inv(Decodable::consensus_decode(&mut raw)?),
            "notfound" => NetworkMessage::NotFound(Decodable::consensus_decode(&mut raw)?),
            "block" => NetworkMessage::Block(Decodable::consensus_decode(&mut raw)?),
            "headers" => {
                let len = VarInt::consensus_decode(&mut raw)?.0;
                let mut headers = Vec::with_capacity(len as usize);
                for _ in 0..len {
                    headers.push(Block::consensus_decode(&mut raw)?.header);
                }
                NetworkMessage::Headers(headers)
            }
            "ping" => NetworkMessage::Ping(Decodable::consensus_decode(&mut raw)?),
            "pong" => NetworkMessage::Pong(Decodable::consensus_decode(&mut raw)?),
            "reject" => NetworkMessage::Reject(Decodable::consensus_decode(&mut raw)?),
            "alert" => NetworkMessage::Alert(Decodable::consensus_decode(&mut raw)?),
            "addr" => NetworkMessage::Addr(Decodable::consensus_decode(&mut raw)?),
            _ => NetworkMessage::Unknown {
                command: self.cmd.clone(),
                payload: raw.to_vec(),
            },
        };
        Ok(payload)
    }

    async fn async_consensus_decode<D: tokio::io::AsyncRead + Unpin>(
        mut d: D,
    ) -> Result<Self, encode::Error> {
        let magic: u32 = Self::read_and_decode(&mut d).await?;
        let cmd: CommandString = Self::read_and_decode(&mut d).await?;

        let len: u32 = Self::read_and_decode(&mut d).await?;
        let _checksum: [u8; 4] = Self::read_and_decode(&mut d).await?;

        let mut raw = vec![0u8; len as usize];
        d.read_exact(&mut raw).await.map_err(encode::Error::Io)?;

        Ok(RawNetworkMessage { magic, cmd, raw })
    }

    async fn read_and_decode<T: Decodable, D: tokio::io::AsyncRead + Unpin>(
        d: &mut D,
    ) -> Result<T, encode::Error> {
        // TODO: Estimate or find a way to determine the exact buffer size needed
        let mut buf = vec![0u8; 1024]; // For demonstration purposes, choose an appropriate size
        d.read_exact(&mut buf).await.map_err(encode::Error::Io)?;

        let mut cursor = std::io::Cursor::new(buf);
        T::consensus_decode(&mut cursor)
    }
}

impl Decodable for RawNetworkMessage {
    fn consensus_decode<D: io::Read + ?Sized>(d: &mut D) -> Result<Self, encode::Error> {
        let magic = Decodable::consensus_decode(d)?;
        let cmd = Decodable::consensus_decode(d)?;

        let len = u32::consensus_decode(d)?;
        let _checksum = <[u8; 4]>::consensus_decode(d)?; // assume data is correct
        let mut raw = vec![0u8; len as usize];
        d.read_slice(&mut raw)?;

        Ok(RawNetworkMessage { magic, cmd, raw })
    }
}

/// `duration_to_seconds` converts Duration to seconds.
#[inline]
pub fn duration_to_seconds(d: Duration) -> f64 {
    let nanos = f64::from(d.subsec_nanos()) / 1e9;
    d.as_secs() as f64 + nanos
}
