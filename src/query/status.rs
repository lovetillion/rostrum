use futures_util::{stream, StreamExt, TryStreamExt};
use std::{
    cmp::Ordering,
    collections::{HashMap, HashSet},
    sync::Arc,
};
use tokio::{join, task::JoinHandle};

use anyhow::*;
use bitcoin_hashes::{hex::ToHex, Hash};
use bitcoincash::Txid;
use rayon::prelude::*;
use sha1::Digest;
use sha2::Sha256;

use crate::{
    chaindef::{OutPointHash, ScriptHash, TokenID},
    indexes::{
        outputindex::OutputIndexRow,
        scripthashindex::{QueryFilter, ScriptHashIndexRow},
        DBRow,
    },
    mempool::{ConfirmationState, Tracker},
    query::{
        queryutil::{
            multi_height_by_txid, outpoint_is_spent, outpoints_are_spent, token_from_outpoint,
        },
        BUFFER_SIZE, CHUNK_SIZE,
    },
    store::{DBContents, DBStore},
};

use super::queryutil::{multi_get_utxo, multi_tx_spending_outpoint};

#[derive(Serialize)]
pub struct HistoryItem {
    pub tx_hash: Txid,
    pub height: i32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fee: Option<u64>, // need to be set only for unconfirmed transactions (i.e. height <= 0)
}

// used only for BCH
#[allow(dead_code)]
fn as_hex<S>(key: &Option<Vec<u8>>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    match key {
        Some(bytes) => serializer.serialize_str(&hex::encode(bytes)),
        None => serializer.serialize_none(),
    }
}

#[derive(Serialize)]
pub struct UnspentItem {
    #[serde(rename = "tx_hash")]
    pub txid: Txid,
    #[serde(rename = "tx_pos")]
    pub vout: u32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub outpoint_hash: Option<OutPointHash>,
    pub height: u32,
    pub value: u64,

    #[cfg(feature = "nexa")]
    #[serde(skip_serializing_if = "Option::is_none", rename = "token_id_hex")]
    pub token_id: Option<TokenID>,

    #[cfg(not(feature = "nexa"))]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub token_id: Option<TokenID>,

    #[cfg(not(feature = "nexa"))]
    #[serde(skip_serializing_if = "Option::is_none", serialize_with = "as_hex")]
    pub commitment: Option<Vec<u8>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub token_amount: Option<i64>,

    /// If this utxo has token in it. Useful when not querying for token info (which adds query cost).
    pub has_token: bool,
}

/**
 * For sorting history items by confirmation height (and then ID)
 */
pub(crate) fn by_block_height(a: &HistoryItem, b: &HistoryItem) -> Ordering {
    if a.height == b.height {
        // Order by little endian tx hash if height is the same,
        // in most cases, this order is the same as on the blockchain.
        return b.tx_hash.cmp(&a.tx_hash);
    }
    if a.height > 0 && b.height > 0 {
        return a.height.cmp(&b.height);
    }

    // mempool txs should be sorted last, so add to it a large number
    // per spec, mempool entries do not need to be sorted, but we do it
    // anyway so that statushash is deterministic
    let mut a_height = a.height;
    let mut b_height = b.height;
    if a_height <= 0 {
        a_height = 0xEE_EEEE + a_height.abs();
    }
    if b_height <= 0 {
        b_height = 0xEE_EEEE + b_height.abs();
    }
    a_height.cmp(&b_height)
}

/**
 * Find output rows given filter parameters.
 *
 * This is an expensive call used by  most blockchain.scripthash.* queries.
 *
 * Returns the utxos + bool indicating if utxo has token
 */
#[allow(clippy::clone_on_copy)]
async fn scan_for_outputs<'a>(
    store: &'a Arc<DBStore>,
    scripthash: ScriptHash,
    filter: &'a QueryFilter,
) -> (
    JoinHandle<Result<()>>,
    impl futures::stream::Stream<Item = (OutputIndexRow, bool)> + 'a,
) {
    let scripthash_filter =
        ScriptHashIndexRow::filter_by_scripthash(scripthash.into_inner(), filter);

    let (query, stream) = store.scan(ScriptHashIndexRow::CF, scripthash_filter).await;

    let stream = stream
        .map(|r| ScriptHashIndexRow::from_row(&r))
        .ready_chunks(CHUNK_SIZE)
        .then(move |r| {
            let store = Arc::clone(store);
            let outpoints: Vec<OutPointHash> = r.iter().map(|r| r.outpointhash()).collect();
            async move {
                let utxos = multi_get_utxo(&store, &outpoints).await;
                stream::iter(
                    utxos
                        .into_iter()
                        .zip(r)
                        .map(|(utxo, r)| (utxo.expect("missing utxo"), r.has_token())),
                )
            }
        })
        .flatten_unordered(BUFFER_SIZE);

    (query, stream)
}

/**
 * Calculate coin balance in scripthash that has been confirmed in blocks.
 */
pub async fn confirmed_scripthash_balance(
    index: &Arc<DBStore>,
    scripthash: ScriptHash,
    filter: QueryFilter,
) -> Result<(i64, Vec<(i64, OutPointHash)>)> {
    assert!(index.contents == DBContents::ConfirmedIndex);

    let (outputs_query, outputs_stream) = scan_for_outputs(index, scripthash, &filter).await;

    let outputs: Vec<(i64, OutPointHash)> = outputs_stream
        .map(|(o, _)| (o.value(), o.take_hash()))
        .ready_chunks(CHUNK_SIZE)
        .then(|outpoints| {
            // Check if outpoints are spent (in baches of CHUNK_SIZE)
            let index = Arc::clone(index);
            async move {
                let (values, outpoints): (Vec<u64>, Vec<OutPointHash>) =
                    outpoints.into_iter().unzip();
                let spent_status = outpoints_are_spent(&index, &outpoints).await;

                stream::iter(values.into_iter().zip(outpoints).zip(spent_status).map(
                    |((value, outpoint), is_spent)| {
                        if is_spent {
                            // Output was created AND spent in the same index. Zero it out.
                            (0, outpoint)
                        } else {
                            (value as i64, outpoint)
                        }
                    },
                ))
            }
        })
        .flatten_unordered(BUFFER_SIZE)
        .collect()
        .await;

    outputs_query.await??;

    let amount = outputs.par_iter().map(|(value, _)| value).sum();

    // Only return unspent confirmed outputs
    let outputs = outputs
        .into_iter()
        .filter(|(amount, _)| amount > &0)
        .collect();

    Ok((amount, outputs))
}

/**
 * Calculate coin balance in scripthash that has been confirmed in blocks.
 *
 * Takes confirmed_outputs in to be able to see if mempool spends confirmed utxos.
 */
pub async fn unconfirmed_scripthash_balance(
    mempool: &Arc<DBStore>,
    confirmed_outputs: Vec<(i64, OutPointHash)>,
    scripthash: ScriptHash,
    filter: QueryFilter,
) -> Result<i64> {
    assert!(DBContents::MempoolIndex == mempool.contents);

    let (outputs_query, outputs_stream) = scan_for_outputs(mempool, scripthash, &filter).await;

    let unconfirmed_outputs: Vec<i64> = outputs_stream
        .map(|(o, _)| (o.value(), o.take_hash()))
        .ready_chunks(CHUNK_SIZE)
        .then(|outpoints| {
            let mempool = mempool.clone();
            async move {
                let (values, outpoints): (Vec<u64>, Vec<OutPointHash>) =
                    outpoints.into_iter().unzip();
                let is_spent = outpoints_are_spent(&mempool, &outpoints).await;
                stream::iter(values.into_iter().zip(is_spent).map(|(value, is_spent)| {
                    if is_spent {
                        // Output was created AND spent in the same index. Zero it out.
                        0
                    } else {
                        value as i64
                    }
                }))
            }
        })
        .flatten_unordered(BUFFER_SIZE)
        .collect()
        .await;

    outputs_query.await??;

    let amount: i64 = unconfirmed_outputs.par_iter().sum();

    // Subtract spends from confirmed utxos
    let spent_confirmed: i64 = stream::iter(confirmed_outputs)
        .ready_chunks(CHUNK_SIZE)
        .then(|outpoints| {
            let mempool = mempool.clone();
            async move {
                let (value, outpoints): (Vec<i64>, Vec<OutPointHash>) =
                    outpoints.into_iter().unzip();
                let is_spent = outpoints_are_spent(&mempool, &outpoints).await;

                stream::iter(value.into_iter().zip(is_spent).map(|(value, is_spent)| {
                    debug_assert!(value >= 0);
                    if is_spent {
                        value
                    } else {
                        0
                    }
                }))
            }
        })
        .flatten_unordered(BUFFER_SIZE)
        .fold(0i64, |acc, value| async move { acc + value })
        .await;

    Ok(amount - spent_confirmed)
}

// Used in .reduce for merging maps
fn merge_token_balance_maps(
    mut a: HashMap<TokenID, i64>,
    b: HashMap<TokenID, i64>,
) -> HashMap<TokenID, i64> {
    for (token_id, amount) in b {
        a.entry(token_id)
            .and_modify(|a| {
                *a += amount;
            })
            .or_insert(amount);
    }
    a
}

/**
 * Calculate token balance in a given store.
 */
async fn calc_token_balance(
    store: &Arc<DBStore>,
    scripthash: ScriptHash,
    filter_token: Option<TokenID>,
) -> Result<(HashMap<TokenID, i64>, Vec<OutPointHash>)> {
    let (outputs_query, outputs_stream) =
        scan_for_outputs(store, scripthash, &QueryFilter::TokenOnly).await;

    let outpoints: Vec<(TokenID, i64, OutPointHash)> = outputs_stream
        .map(|(o, _)| o.take_hash())
        .map(|outpoint| async move {
            let (token_future, is_spent) = join!(
                token_from_outpoint(store, &outpoint),
                outpoint_is_spent(store, outpoint),
            );
            let (token_id, amount, _) = token_future?.context(format!(
                "token info not found for outpoint {}",
                outpoint.to_hex()
            ))?;

            let valid_amount = if amount < 0 || is_spent { 0 } else { amount };
            Ok((token_id, valid_amount, outpoint))
        })
        .buffer_unordered(BUFFER_SIZE)
        .try_collect::<Vec<(TokenID, i64, OutPointHash)>>()
        .await?;

    outputs_query.await??;

    let outpoints = if let Some(filter) = filter_token {
        outpoints
            .into_iter()
            .filter(|(token_id, _, _)| token_id.eq(&filter))
            .collect()
    } else {
        outpoints
    };

    let outpoints_len = outpoints.len();

    let (balance, outputs) = outpoints.into_iter().fold(
        (
            HashMap::<TokenID, i64>::default(),
            Vec::with_capacity(outpoints_len),
        ),
        |(mut map, mut ops): (HashMap<TokenID, i64>, Vec<OutPointHash>),
         (token_id, amount, outpoint)| {
            map.entry(token_id)
                .and_modify(|a| *a += amount)
                .or_insert(amount);
            ops.push(outpoint);
            (map, ops)
        },
    );

    Ok((balance, outputs))
}

/**
 * Get the unconfirmed token balance
 */
pub(crate) async fn unconfirmed_scripthash_token_balance(
    mempool: &Arc<DBStore>,
    index: &Arc<DBStore>,
    confirmed_outputs: Vec<OutPointHash>,
    scripthash: ScriptHash,
    filter_token: Option<TokenID>,
) -> Result<HashMap<TokenID, i64>> {
    assert!(mempool.contents == DBContents::MempoolIndex);
    assert!(index.contents == DBContents::ConfirmedIndex);

    // This finds the balance of entries that have been both created and spent
    // in the mempool. It cannot see spends of outputs thave have been confirmed.
    let (balance, _) = calc_token_balance(mempool, scripthash, filter_token).await?;

    // Find inputs that spends from confirmed outputs as well
    let spends_of_confirmed: Vec<(TokenID, i64)> = stream::iter(confirmed_outputs)
        .then(|outpoint| async move {
            if outpoint_is_spent(mempool, outpoint).await {
                let (token_id, amount, _) = token_from_outpoint(index, &outpoint)
                    .await?
                    .expect("token info found for outpoint {outpoint}");
                Ok((token_id, -amount))
            } else {
                // This output was funded in the confirmed index, not mempool.
                // If it's not spent here, then ignore it.
                Ok((TokenID::all_zeros(), 0))
            }
        })
        .try_collect()
        .await?;

    let spends_of_confirmed = spends_of_confirmed.into_iter().fold(
        HashMap::<TokenID, i64>::default(),
        |mut map, (token_id, amount)| {
            map.entry(token_id)
                .and_modify(|a| {
                    *a += amount;
                })
                .or_insert(amount);
            map
        },
    );

    // Merge in spends of confirmed outputs
    let mut balance = merge_token_balance_maps(balance, spends_of_confirmed);
    balance.remove(&TokenID::all_zeros());

    Ok(balance)
}

/**
 * Get the confirmed token balance
 */
pub(crate) async fn confirmed_scripthash_token_balance(
    store: &Arc<DBStore>,
    scripthash: ScriptHash,
    filter_token: Option<TokenID>,
) -> Result<(HashMap<TokenID, i64>, Vec<OutPointHash>)> {
    calc_token_balance(store, scripthash, filter_token).await
}

/**
 * Find all transactions that spend of fund scripthash.
 *
 * Parameter 'additional_outpoints' is for funding utxos from other indexes.
 * For mempool, this parameter is needed to be able to locate spends of confirmed outputs
 * (as those outputs are not funded in the mempool)
 */
pub(crate) async fn scripthash_transactions(
    store: &Arc<DBStore>,
    scripthash: ScriptHash,
    filter: QueryFilter,
    filter_token: Option<TokenID>,
    additional_outputs: Vec<OutPointHash>,
) -> Result<(HashSet<Txid>, Vec<OutPointHash>)> {
    let (outputs_query, outputs_stream) = scan_for_outputs(store, scripthash, &filter).await;

    let history: Vec<(Txid, OutPointHash)> = outputs_stream
        .then(|(o, _)| {
            // If a token filter is provided, then we need a copy of store
            // for token lookup.
            let store = filter_token.as_ref().map(|_| store.clone());
            let outpointhash = o.hash();
            async move {
                if let Some(db) = store {
                    let token = token_from_outpoint(&db, &outpointhash).await?;
                    let (token, _, _) = token.context("expected token it outpoint")?;
                    Ok((o.txid(), outpointhash, Some(token)))
                } else {
                    Ok((o.txid(), outpointhash, None))
                }
            }
        })
        .try_filter_map(|(txid, outpoint, token)| {
            let keep = if let Some(f) = filter_token.as_ref() {
                f.eq(&token.unwrap())
            } else {
                true
            };

            futures::future::ready(Ok(if keep { Some((txid, outpoint)) } else { None }))
        })
        .try_collect()
        .await?;

    outputs_query.await??;

    // There is no "try_unzip", so need to do this in a separate iteration
    let (mut funder_txid, outpoints): (HashSet<Txid>, Vec<OutPointHash>) =
        history.into_iter().unzip();

    // Unfortunetly, we ened a copy, because without it, we run into "higher-ranked lifetime error"
    // when using it in the async closure below.
    let outpoints_cpy = outpoints.clone();

    let spender_txids: HashSet<Txid> = stream::iter(outpoints_cpy.into_iter())
        .chain(stream::iter(additional_outputs.into_iter()))
        .ready_chunks(CHUNK_SIZE)
        .then(|outpoints| {
            let store = store.clone();
            async move {
                stream::iter(
                    multi_tx_spending_outpoint(&store, &outpoints)
                        .await
                        .into_iter()
                        .filter_map(|i| i.map(|i| i.txid())),
                )
            }
        })
        .flatten_unordered(BUFFER_SIZE)
        .collect()
        .await;

    funder_txid.extend(spender_txids);
    Ok((funder_txid, outpoints))
}

/**
 * Get outputs that have been confirmed on the blockchain given filter parameters.
 */
pub(crate) async fn get_confirmed_outputs(
    index: &Arc<DBStore>,
    scripthash: ScriptHash,
    filter: QueryFilter,
    filter_token: Option<TokenID>,
) -> Result<Vec<OutPointHash>> {
    assert!(index.contents == DBContents::ConfirmedIndex);

    let (query, stream) = scan_for_outputs(index, scripthash, &filter).await;

    let outpoints = stream
        .map(|(o, _)| o.hash())
        .then(|o| {
            // We only need the index when filtering on token.
            let index = filter_token.as_ref().map(|_| index.clone());
            async move {
                match index {
                    Some(db) => {
                        let token = token_from_outpoint(&db, &o).await?;
                        Ok((o, token.map(|(t, _, _)| t)))
                    }
                    None => Ok((o, None)),
                }
            }
        })
        .try_filter_map(|(o, token)| {
            let keep = match filter_token.as_ref() {
                Some(filter) => {
                    match token {
                        Some(t) => t.eq(filter),
                        None => {
                            // utxo without token
                            false
                        }
                    }
                }
                None => true, // not filtered
            };
            futures::future::ready(Ok(if keep { Some(o) } else { None }))
        })
        .try_collect()
        .await?;

    query.await??;
    Ok(outpoints)
}

/**
 * Generate list of HistoryItem for a scripthashes confirmed history.
 *
 * Also returns confirmed outputs that can be used to get unconfirmed spends
 * of confirmed utxos.
 */
async fn confirmed_history(
    store: &Arc<DBStore>,
    scripthash: ScriptHash,
    filter: QueryFilter,
    filter_token: Option<TokenID>,
) -> Result<(Vec<HistoryItem>, Vec<OutPointHash>)> {
    let (txids, outputs) =
        scripthash_transactions(store, scripthash, filter, filter_token, vec![]).await?;

    let history = stream::iter(txids)
        .chunks(CHUNK_SIZE)
        .then(|txids| {
            let store = store.clone();
            async move {
                let heights = multi_height_by_txid(&store, &txids).await;

                stream::iter(txids.into_iter().zip(heights).map(|(txid, height)| {
                    let height = match height {
                        Some(h) => h as i32,
                        None => {
                            debug_assert!(false, "Confirmed tx cannot be missing height");
                            0
                        }
                    };

                    HistoryItem {
                        tx_hash: txid,
                        height,
                        fee: None,
                    }
                }))
            }
        })
        .flatten_unordered(BUFFER_SIZE)
        .collect()
        .await;
    Ok((history, outputs))
}

/**
 * Generate list of HistoryItem for a scripthashes unconfirmed history
 */
pub async fn unconfirmed_history(
    mempool: &Tracker,
    confirmed_outputs: Vec<OutPointHash>,
    scripthash: ScriptHash,
    filter: QueryFilter,
    filter_token: Option<TokenID>,
) -> Result<Vec<HistoryItem>> {
    let (txids, _) = scripthash_transactions(
        mempool.index(),
        scripthash,
        filter,
        filter_token,
        confirmed_outputs,
    )
    .await?;

    Ok(stream::iter(txids)
        .map(|txid| async move {
            let height = match mempool.tx_confirmation_state(&txid, None).await {
                ConfirmationState::InMempool => 0,
                ConfirmationState::UnconfirmedParent => -1,
                ConfirmationState::Indeterminate | ConfirmationState::Confirmed => {
                    debug_assert!(
                        false,
                        "Mempool tx's state cannot be indeterminate or confirmed"
                    );
                    0
                }
            };
            HistoryItem {
                tx_hash: txid,
                height,
                fee: mempool.get_fee(&txid).await,
            }
        })
        .buffer_unordered(BUFFER_SIZE)
        .collect()
        .await)
}

pub async fn scripthash_history(
    store: &Arc<DBStore>,
    mempool: &Tracker,
    scripthash: ScriptHash,
    filter: QueryFilter,
    filter_token: Option<TokenID>,
) -> Result<Vec<HistoryItem>> {
    let (mut history, confirmed_outputs) =
        confirmed_history(store, scripthash, filter, filter_token.clone()).await?;

    history.extend(
        unconfirmed_history(mempool, confirmed_outputs, scripthash, filter, filter_token).await?,
    );

    Ok(tokio::task::spawn_blocking(|| async move {
        history.par_sort_unstable_by(by_block_height);
        history
    })
    .await
    .unwrap()
    .await)
}

/**
 * Generate a hash of scripthash history as defined in electrum spec
 */
pub fn hash_scripthash_history(history: &Vec<HistoryItem>) -> Option<[u8; 32]> {
    if history.is_empty() {
        None
    } else {
        let mut sha2 = Sha256::new();
        let parts: Vec<String> = history
            .into_par_iter()
            .map(|t| format!("{}:{}:", t.tx_hash.to_hex(), t.height))
            .collect();

        for p in parts {
            sha2.update(p.as_bytes());
        }
        Some(sha2.finalize().into())
    }
}

pub(crate) async fn scripthash_listunspent(
    store: &Arc<DBStore>,
    mempool: &Arc<DBStore>,
    scripthash: ScriptHash,
    filter: QueryFilter,
    filter_token: Option<TokenID>,
) -> Result<Vec<UnspentItem>> {
    assert!(store.contents == DBContents::ConfirmedIndex);
    assert!(mempool.contents == DBContents::MempoolIndex);

    if filter_token.is_some() {
        assert!(filter == QueryFilter::TokenOnly)
    }

    let (confirmed_query, confirmed_stream) = scan_for_outputs(store, scripthash, &filter).await;

    let confirmed =
        confirmed_stream
            .ready_chunks(CHUNK_SIZE)
            .then(|outputs| {
                // Filter out spends
                let outpoints: Vec<OutPointHash> = outputs.iter().map(|(o, _)| o.hash()).collect();
                let confirmed = store.clone();
                let unconfirmed = mempool.clone();
                async move {
                    let is_spent = outpoints_are_spent(&confirmed, &outpoints).await;
                    let outputs: Vec<(OutputIndexRow, bool)> = outputs
                        .into_iter()
                        .zip(is_spent)
                        .filter_map(|(o, is_spent)| if is_spent { None } else { Some(o) })
                        .collect();
                    let outpoints: Vec<OutPointHash> =
                        outputs.iter().map(|(o, _)| o.hash()).collect();
                    let is_spent = outpoints_are_spent(&unconfirmed, &outpoints).await;
                    stream::iter(
                        outputs
                            .into_iter()
                            .zip(is_spent)
                            .filter_map(|(o, is_spent)| if is_spent { None } else { Some(o) }),
                    )
                }
            })
            .flatten_unordered(None)
            .ready_chunks(CHUNK_SIZE)
            .then(|outputs| {
                let store = Arc::clone(store);
                async move {
                    let txids: Vec<Txid> = outputs.iter().map(|(o, _)| o.txid()).collect();
                    let heights = multi_height_by_txid(&store, &txids).await;
                    stream::iter(outputs.into_iter().zip(heights).map(
                        |((out, has_token), height)| {
                            (out, height.expect("height not found"), has_token)
                        },
                    ))
                }
            })
            .flatten_unordered(BUFFER_SIZE);

    let (unconfirmed_query, unconfirmed_stream) =
        scan_for_outputs(mempool, scripthash, &filter).await;

    let unconfirmed = unconfirmed_stream
        .ready_chunks(CHUNK_SIZE)
        .then(|outputs| {
            let outpoints: Vec<OutPointHash> = outputs.iter().map(|(o, _)| o.hash()).collect();
            let mempool = Arc::clone(mempool);
            async move {
                let is_spent = outpoints_are_spent(&mempool, &outpoints).await;
                stream::iter(outputs.into_iter().zip(is_spent).map(|(o, is_spent)| {
                    if is_spent {
                        None
                    } else {
                        Some((o.0, 0 /* mempool height */, o.1))
                    }
                }))
            }
        })
        .flatten_unordered(BUFFER_SIZE)
        .filter_map(|item| async move { item });

    let unspent_outputs = confirmed.chain(unconfirmed);

    // Fetch token info & filter if token_filter is set.
    let unspent_outputs = unspent_outputs.filter_map(|(output, height, has_token)| {
        let filter_token = filter_token.clone();

        // Take a copies for async call if user requested token info or filters on token.
        let (confirmed, unconfirmed) = if has_token && filter == QueryFilter::TokenOnly {
            (Some(Arc::clone(store)), Some(Arc::clone(mempool)))
        } else {
            (None, None)
        };

        async move {
            if !has_token || filter != QueryFilter::TokenOnly {
                // No filter
                return Some(Ok((output, height, None, None, has_token, None)));
            }

            let outpointhash = output.hash();
            let token = match token_from_outpoint(&confirmed.unwrap(), &outpointhash).await {
                std::result::Result::Ok(t) => t,
                Err(e) => return Some(Err(e)),
            };
            let (token_id, amount, commitment) = if let Some(t) = token {
                t
            } else {
                // Try mempool.
                let t = match token_from_outpoint(&unconfirmed.unwrap(), &outpointhash).await {
                    std::result::Result::Ok(t) => t,
                    Err(e) => return Some(Err(e)),
                };
                match t {
                    Some(t) => t,
                    None => return Some(Err(anyhow!("expected to find token output"))),
                }
            };

            if let Some(filter) = &filter_token {
                if filter == &token_id {
                    Some(Ok((
                        output,
                        height,
                        Some(token_id),
                        Some(amount),
                        has_token,
                        commitment,
                    )))
                } else {
                    None
                }
            } else {
                // Did not filter on specific token.
                Some(Ok((
                    output,
                    height,
                    Some(token_id),
                    Some(amount),
                    has_token,
                    commitment,
                )))
            }
        }
    });

    #[allow(unused_variables)] // commitment unused on nexa (it's part of token_id)
    #[allow(clippy::type_complexity)]
    let unspent_outputs = unspent_outputs.map(|output| {
        let (out, height, token_id, token_amount, has_token, commitment): (
            OutputIndexRow,
            u32,
            Option<TokenID>,
            Option<i64>,
            bool,
            Option<Vec<u8>>,
        ) = output?;
        #[cfg(feature = "nexa")]
        {
            // With nexa, we also want outpoint hash
            Ok(UnspentItem {
                txid: out.txid(),
                vout: out.index(),
                outpoint_hash: Some(out.hash()),
                height,
                value: out.value(),
                token_id,
                token_amount,
                has_token,
            })
        }
        #[cfg(not(feature = "nexa"))]
        Ok({
            UnspentItem {
                txid: out.txid(),
                vout: out.index(),
                outpoint_hash: None,
                height,
                value: out.value(),
                token_id,
                token_amount,
                has_token,
                commitment,
            }
        })
    });

    let mut unspent: Vec<UnspentItem> = unspent_outputs.try_collect().await?;
    confirmed_query.await??;
    unconfirmed_query.await??;

    unspent.par_sort_by_key(|u| u.height);

    Ok(unspent)
}
