use anyhow::Result;
use std::future::Future;
use tokio::task::JoinHandle;

pub fn spawn<F>(name: &'static str, f: F) -> std::thread::JoinHandle<()>
where
    F: 'static + Send + FnOnce() -> Result<()>,
{
    std::thread::Builder::new()
        .name(name.to_owned())
        .spawn(move || {
            if let Err(e) = f() {
                warn!("{} thread failed: {}", name, e);
            }
        })
        .expect("failed to spawn a thread")
}

pub fn spawn_task<T, F>(name: &'static str, future: F) -> JoinHandle<()>
where
    T: Send + 'static,
    F: Future<Output = Result<T>> + Send + 'static,
{
    tokio::task::spawn(async move {
        if let Err(err) = future.await {
            warn!("task '{}' failed: {}", name, err)
        }
    })
}
